import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';




import 'package:happy_breakfast/view_dev_giang/presentation/cart/cart.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_itemchoose.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_infomation.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_options.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_tracking.dart';

import 'package:happy_breakfast/view_dev_giang/presentation/profile/widget/custom_notification.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/rate/create_rate.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/rate/rate_detail_product.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/rate/rate_order.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/voucher/voucher.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/widget/color_styles.dart';

class ProfileSrc extends StatefulWidget {
  const ProfileSrc({super.key});

  @override
  State<ProfileSrc> createState() => _ProfileSrcState();
}

class _ProfileSrcState extends State<ProfileSrc> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 55),
            height: 164,
            decoration: const BoxDecoration(color: primaryColor),
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const RateOrderSrc()));
                        },
                        child: const CustomNotification(
                            color: Colors.white,
                            icon: Icons.notifications_none, textCount: "99+"),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const CartSrc()));
                        },
                        child: const CustomNotification(
                            icon: Icons.shopping_cart_outlined,
                            color: Colors.white,
                            textCount: "99+"),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Row(
                          children: [
                            Stack(
                              children: [
                                Positioned(
                                  child: Container(
                                    height: 64,
                                    width: 64,
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/avatar2.png"),
                                            fit: BoxFit.cover)),
                                    child: Container(),
                                  ),
                                ),
                                Positioned(
                                    top: 44,
                                    left: 44,
                                    child: GestureDetector(
                                      onTap: () {
                                        print("object");
                                      },
                                      child: Container(
                                          height: 20,
                                          width: 20,
                                          decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Color(0xffF5F5F5)),
                                          child: const Icon(
                                            Icons.camera_alt_outlined,
                                            size: 14,
                                          )),
                                    ))
                              ],
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Xin chào",
                                    style: TextStyle(
                                        color: Color(0xfffffefa),
                                        fontWeight: FontWeight.w400)),
                                Text("Nguyễn Tú Anh",
                                    style: TextStyle(
                                        color: Color(0xfffffefa),
                                        fontWeight: FontWeight.w500))
                              ],
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Container(
                            height: 30,
                            width: 118,
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            decoration: const BoxDecoration(
                                color: Color(0xfffffefa),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: GestureDetector(
                              onTap: () {},
                              child: const Row(
                                children: [
                                  Text(
                                    "Thông tin tài khoản",
                                    style: TextStyle(
                                        color: primaryColor,
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 12,
                                    color: primaryColor,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const OrderOptionSrc()));
            },
            child: const CustomItem(
              leading: Icons.monetization_on_outlined,
              color: Color(0xff26A44D),
              text: "Đơn hàng",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const VoucherSrc()));
            },
            child: const CustomItem(
              leading: Icons.account_balance_wallet_outlined,
              color: Color(0xffB62C21),
              text: "Voucher",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const OrderInformation()));
            },
            child: const CustomItem(
              leading: Icons.real_estate_agent_outlined,
              color: Color(0xff4F5E9F),
              text: "Thông tin đơn hàng",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const OrderTrackingSrc()));
            },
            child: const CustomItem(
              leading: Icons.handshake_outlined,
              color: Color(0xff11B05C),
              text: "Tracking",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CreateRateSrc()));
            },
            child: const CustomItem(
              leading: Icons.settings_outlined,
              color: Color(0xff242424),
              text: "Tạo đánh giá",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const RateDetailProductSrc()));
            },
            child: const CustomItem(
              leading: Icons.shopping_bag_outlined,
              color: Color(0xffFFC300),
              text: "Chi tiết đánh giá",
              textTotal: "",
            ),
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          const CustomItem(
            leading: Icons.description_outlined,
            color: Color(0xff0A6836),
            text: "Chính sách",
            textTotal: "",
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          const CustomItem(
            leading: Icons.headset_mic_outlined,
            color: Color(0xff4F5E9F),
            text: "Liên hệ",
            textTotal: "",
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),
          const CustomItem(
            leading: Icons.logout,
            color: Color(0xffDA3E31),
            text: "Đăng nhập",
            textTotal: "",
          ),
          const Divider(height: 1, color: Colors.grey, thickness: 0.2),

          Column(
            children: [
              const CustomItem(
                leading: Icons.monetization_on_outlined,
                color: Color(0xff26A44D),
                text: "Điểm thưởng",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
                ,
              const CustomItem(
                leading: Icons.account_balance_wallet_outlined,
                color: Color(0xffB62C21),
                text: "Voucher",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
                 ,
              const CustomItem(
                leading: Icons.real_estate_agent_outlined,
                color: Color(0xff4F5E9F),
                text: "Cộng đồng",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
             ,
              const CustomItem(
                leading: Icons.handshake_outlined,
                color: Color(0xff11B05C),
                text: "Đăng ký CTV",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
        ,
              const CustomItem(
                leading: Icons.settings_outlined,
                color: Color(0xff242424),
                text: "Cài đặt",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
             ,
              const CustomItem(
                leading: Icons.shopping_bag_outlined,
                color: Color(0xffFFC300),
                text: "Mua lại",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
             ,
              const CustomItem(
                leading: Icons.description_outlined,
                color: Color(0xff0A6836),
                text: "Chính sách",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
                  ,
              const CustomItem(
                leading: Icons.headset_mic_outlined,
                color: Color(0xff4F5E9F),
                text: "Liên hệ",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
                 ,
              const CustomItem(
                leading: Icons.logout,
                color: Color(0xffDA3E31),
                text: "Đăng xuất",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
              ,
              const CustomItem(
                leading: Icons.logout,
                color: Color(0xffDA3E31),
                text: "Đăng xuất",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
              ,
              const CustomItem(
                leading: Icons.logout,
                color: Color(0xffDA3E31),
                text: "Đăng xuất",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
          ,
              const CustomItem(
                leading: Icons.logout,
                color: Color(0xffDA3E31),
                text: "Đăng xuất",
                textTotal: "",
              ),
              const Divider(height: 1, color: Colors.grey, thickness: 0.2)
                  ,
            ],
          ),

        ],
      ),
    );
  }
}
