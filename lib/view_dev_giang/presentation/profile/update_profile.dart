import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_textfield.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_textfield_update.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/widget/color_styles.dart';
import 'package:intl/intl.dart';

class UpdateProfileSrc extends StatefulWidget {
  const UpdateProfileSrc({super.key});

  @override
  State<UpdateProfileSrc> createState() => _UpdateProfileSrcState();
}

class _UpdateProfileSrcState extends State<UpdateProfileSrc> {
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController date = TextEditingController();
  int _selectedValue = 1;

  void _handleRadioValueChange(int? value) {
    setState(() {
      _selectedValue = value ?? 1;
    });
  }

  Widget buildRadioBox() {
    return SizedBox(
      height: 24,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Row(
              children: [
                SizedBox(
                  child: Radio(
                    activeColor: ColorAppStyle.button,
                    value: 1,
                    groupValue: _selectedValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                const Text("Nam"),
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                SizedBox(
                  child: Radio(
                    activeColor: ColorAppStyle.button,
                    value: 2,
                    groupValue: _selectedValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                const Text("Nữ"),
              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                SizedBox(
                  child: Radio(
                    activeColor: ColorAppStyle.button,
                    value: 3,
                    groupValue: _selectedValue,
                    onChanged: _handleRadioValueChange,
                  ),
                ),
                const Text("Khác"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(
            height: 26,
            width: 26,
            child: IconButton(
                onPressed: () {}, icon: const Icon(Icons.arrow_back))),
        title: const Text(
          "Cập nhật thông tin",
          style: TextStyle(
              fontSize: 16,
              color: Color(0xff1f1f1f),
              fontWeight: FontWeight.w700),
        ),
      ),
      body: Center(
        child: SizedBox(
          height: 812,
          width: 375,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                  width: 339,
                  child: Text(
                    "Vui lòng nhập đầy đủ thông tin dưới đây để hoàn tất đăng ký",
                    style: TextStyle(
                        fontFamily: "Roboto",
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff6b6b6b)),
                  ),
                ),
                const SizedBox(
                    height: 24,
                    width: 339,
                    child: Text(
                      "Thông tin tài khoản",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto",
                          color: Color(0xff1f1f1f)),
                    )),
                Stack(
                  children: [
                    Positioned(
                      child: Container(
                        height: 109,
                        width: 103,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage("assets/images/avatar2.png"),
                                fit: BoxFit.cover)),
                        child: Container(),
                      ),
                    ),
                    Positioned(
                        width: 32,
                        height: 32,
                        top: 77,
                        left: 66,
                        child: Container(
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xffF5F5F5)),
                            child: IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.camera_alt_outlined,
                                size: 14,
                              ),
                            )))
                  ],
                ),
                SizedBox(
                  height: 300,
                  width: 343,
                  child: Column(
                    children: [
                      CustomTextFieldUpdate(
                        text: "Họ và tên",
                        controller: name,
                        hintText: "Nhập họ và tên",
                      ),
                      CustomTextFieldUpdate(
                        text: "Địa chỉ",
                        controller: address,
                        hintText: "Nhập địa chỉ",
                      ),
                      CustomTextFieldUpdate(
                        text: "Ngày sinh",
                        controller: date,
                        hintText: "dd/mm/yy",
                        icon: IconButton(
                            onPressed: () async {
                              late final formatter = DateFormat('dd/MM/yyyy');
                              DateTime? picker = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(2000),
                                  lastDate: DateTime(2030));
                              setState(() {
                                date.text = formatter.format(picker!);
                              });
                            },
                            icon: const Icon(Icons.calendar_month)),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Giới tính",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Roboto",
                                color: Color(0xff1f1f1f)),
                          ),
                          buildRadioBox(),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 338,
                  height: 44,
                  decoration: const BoxDecoration(
                      color: ColorAppStyle.button,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: TextButton(
                    onPressed: () {},
                    child: const Text(
                      "Xác nhận",
                      style: TextStyle(
                          color: Color(0xfffffefa),
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Roboto"),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
