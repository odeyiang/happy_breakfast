import 'package:flutter/material.dart';

class CustomNotification extends StatelessWidget {
  final IconData icon;
  final String textCount;
  final Color? color;
  const CustomNotification({super.key, required this.icon, required this.textCount, this.color});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 38,
      height: 27,
      child: Stack(
        children: [
          Positioned(
              width: 24,
              height: 24,
              child: Icon(icon,color: color,)),
          Positioned(
              left: 12,
              child: Container(
                height: 17,
                width: 26,
                decoration:
                const BoxDecoration(color: Color(0xffFFB41F),
                    borderRadius: BorderRadius.all(Radius.circular(9))),
                child: Center(
                  child: Text(
                    textCount,
                    style: const TextStyle(
                        color: Color(0xfffffefa), fontSize: 11),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
