import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/model/product_model.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/payment.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/voucher/voucher.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/widget/color_styles.dart';
import 'package:flutter/services.dart' show rootBundle;

class CartSrc extends StatefulWidget {
  const CartSrc({super.key});

  @override
  State<CartSrc> createState() => _CartSrcState();
}

class _CartSrcState extends State<CartSrc> {
  int countProduct = 1;

  Future<List<Product>> fetchData() async {
    String jsonData = await rootBundle.loadString('assets/product.json');

    List<dynamic> jsonList = json.decode(jsonData);
    List<Product> dataList =
        jsonList.map((json) => Product.fromJson(json)).toList();
    return dataList;
  }

  bool _masterCheckboxValue = false;
  bool _checkbox = false;
  int? selectIndex;

  List<bool> _otherCheckboxValues = List.generate(5, (index) => false);
  List<bool> saveStatus = [];

  void _toggleAllCheckboxes(bool? value) {
    setState(() {
      _masterCheckboxValue = value!;
      _otherCheckboxValues = List.generate(5, (index) => value);
      _checkbox = value;
    });
  }

  // void _toggleCheckbox(int index, bool value) {
  //   setState(() {
  //     _masterCheckboxValue = value;
  //     _masterCheckboxValue = _otherCheckboxValues.every((value) => value);
  //   });
  // }

  List<String> types = ["Vị ổi", "Đào", "Mơ", "Mắm tôm", "Sucream", "Táo"];

  Widget buildListType() {
    return GridView.builder(
      itemCount: types.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 2.5),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: const BoxDecoration(
              color: Color(0xff0A6836),
              borderRadius: BorderRadius.all(Radius.circular(8))),
          margin: const EdgeInsets.all(10),
          child: Center(
            child: Text(
              types[index],
              style: const TextStyle(
                  color: Color(0xfffffefa),
                  fontWeight: FontWeight.w500,
                  fontSize: 16),
            ),
          ),
        );
      },
    );
  }

  Widget buildDetail() {
    return SizedBox(
      height: 340,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 200,
            width: 350,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  "Chi tiết khuyến mại",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Tổng tiền hàng",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    Text("${Const.convertPrice(1000000)}đ")
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Giảm giá sản phẩm",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    Text("-${Const.convertPrice(600000)}đ")
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Tiết kiệm",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    Text("${Const.convertPrice(600000)}đ")
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Tổng số tiền",
                        style: TextStyle(fontWeight: FontWeight.w500)),
                    Text(
                      "${Const.convertPrice(400000)}đ",
                      style: const TextStyle(
                          color: Color(0xff0a6836),
                          fontWeight: FontWeight.w700,
                          fontSize: 18),
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            margin: const EdgeInsets.only(left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const VoucherSrc()));
                  },
                  child: const SizedBox(
                    child: Row(
                      children: [
                        Icon(Icons.local_offer_outlined,
                            color: Color(0xffDA3E31), size: 14),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Voucher của bạn",
                          style: TextStyle(fontSize: 10),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                    height: 20,
                    width: 140,
                    margin: const EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color(0xff0a6836), width: 0.2)),
                    child: const Center(
                      child: Text(
                        "Giảm 10% mã vận chuyển",
                        style:
                            TextStyle(color: Color(0xff0a6836), fontSize: 10),
                      ),
                    )),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        const Text(
                          "Tổng thanh toán: ",
                          style: TextStyle(
                              color: Color(0xff8585858), fontSize: 12),
                        ),
                        Text(
                          "${Const.convertPrice(400000)}đ",
                          style: const TextStyle(
                              color: Color(0xff0a6836),
                              fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Tiết kiệm "),
                        Text(
                          "600k",
                          style: TextStyle(
                              color: Color(0xffda3e31),
                              fontWeight: FontWeight.w400),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 4,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const PaymentSrc()));
                  },
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    width: 135,
                    height: 50,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8),
                        ),
                        color: Color(0xff0a6836)),
                    child: const Center(
                      child: Text(
                        "Xác nhận",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Color(0xfffffefa)),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildList() {
    return FutureBuilder(
      future: fetchData(),
      builder: (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          List<Product> productList = snapshot.data!;
          return ListView.builder(
            itemCount: productList.length,
            itemBuilder: (context, index) {
              List<Product> productList = snapshot.data!;
              return Container(
                margin: const EdgeInsets.symmetric(vertical: 8),
                height: 185,
                decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Transform.scale(
                          scale: 1.3,
                          child: Checkbox(
                            side: const BorderSide(
                                width: 0.5, color: Colors.grey),
                            shape: const CircleBorder(),
                            activeColor: primaryColor,
                            value: _otherCheckboxValues[index],
                            onChanged: (bool? value) async {
                              setState(() {
                                _otherCheckboxValues[index] = value!;
                                saveStatus.clear();
                                saveStatus.addAll(_otherCheckboxValues);
                                bool check = saveStatus.contains(true);
                                if (check) {
                                  _checkbox = true;
                                } else {
                                  _checkbox = false;
                                }

                                //   _toggleCheckbox(index, value);
                                // if (value != null && value) {
                                //   selectIndex = index;
                                //   print(selectIndex);
                                // } else {
                                //   selectIndex = null;
                                // }
                              });
                            },
                          ),
                        ),
                        Stack(
                          children: [
                            Container(
                              height: 64,
                              width: 64,
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(8)),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "${productList[index].imagePath}"))),
                            )
                          ],
                        )
                      ],
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: 250,
                          child: Text(
                            "${productList[index].productName}",
                            style: const TextStyle(fontSize: 14),
                          ),
                        ),
                        Text("SKU: ${productList[index].codeSku}"),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          height: 26,
                          decoration: const BoxDecoration(
                              color: primaryColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          child: Center(
                              child: GestureDetector(
                            onTap: () {
                              showModalBottomSheet<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return SizedBox(
                                    height: 250,
                                    child: Column(
                                      children: [
                                        const SizedBox(
                                          height: 30,
                                        ),
                                        const Text(
                                          "Phân Loại",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        const Divider(
                                          height: 2,
                                          endIndent: 10,
                                          indent: 10,
                                        ),
                                        Expanded(child: buildListType()),
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            child: Text(
                              "Phân loại: ${productList[index].type}",
                              style: const TextStyle(
                                  color: Color(0xfffafafa),
                                  fontWeight: FontWeight.w400),
                            ),
                          )),
                        ),
                        Row(
                          children: [
                            Text(
                                "${Const.convertPrice(productList[index].price)}đ",
                                style: const TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    decorationColor: Color(0xffa3a2a2),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xffa3a2a2))),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${Const.convertPrice(productList[index].salePrice)}đ",
                              style: const TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: primaryColor),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              width: 88,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 0.2,
                                      color: const Color(0xffD2D2D2))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  const Icon(
                                    Icons.remove_sharp,
                                    size: 14,
                                  ),
                                  Text(
                                    "$countProduct",
                                    style: const TextStyle(fontSize: 14),
                                  ),
                                  const Icon(
                                    Icons.add,
                                    size: 14,
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(width: 100),
                            const Icon(
                              Icons.delete_outline,
                              color: Color(0xffB62C21),
                            )
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              );
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          "Giỏ hàng",
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Color(0xff1f1f1f)),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Expanded(child: buildList()),
            SizedBox(
              height: 140,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 40,
                    margin: const EdgeInsets.only(left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const VoucherSrc()));
                          },
                          child: const SizedBox(
                            child: Row(
                              children: [
                                Icon(Icons.local_offer_outlined,
                                    color: Color(0xffDA3E31), size: 14),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Voucher của bạn",
                                  style: TextStyle(fontSize: 10),
                                )
                              ],
                            ),
                          ),
                        ),
                        _checkbox
                            ? Container(
                                height: 20,
                                width: 140,
                                margin: const EdgeInsets.only(right: 5),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: const Color(0xff0a6836),
                                        width: 0.2)),
                                child: const Center(
                                  child: Text(
                                    "Giảm 10% mã vận chuyển",
                                    style: TextStyle(
                                        color: Color(0xff0a6836), fontSize: 10),
                                  ),
                                ))
                            : Container(),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: SizedBox(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  const Text(
                                    "Tổng thanh toán: ",
                                    style: TextStyle(
                                        color: Color(0xff8585858),
                                        fontSize: 12),
                                  ),
                                  Text(
                                    "${Const.convertPrice(400000)}đ",
                                    style: const TextStyle(
                                        color: Color(0xff0a6836),
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Transform.scale(
                                        scale: 1.2,
                                        child: Checkbox(
                                          activeColor: primaryColor,
                                          side: const BorderSide(
                                              width: 3, color: Colors.grey),
                                          shape: const CircleBorder(),
                                          value: _masterCheckboxValue,
                                          onChanged: _toggleAllCheckboxes,
                                        ),
                                      ),
                                      const Text(
                                        "Tất cả",
                                        style: TextStyle(
                                            color: Color(0xff1f1f1f),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                  const Row(
                                    children: [
                                      Text("Tiết kiệm "),
                                      Text(
                                        "600k",
                                        style: TextStyle(
                                            color: Color(0xffda3e31),
                                            fontWeight: FontWeight.w400),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: GestureDetector(
                          onTap: () {
                            _checkbox
                                ? showModalBottomSheet<void>(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return buildDetail();
                                    },
                                  )
                                : null;
                          },
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 5),
                            width: 135,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(8),
                                ),
                                color: _checkbox
                                    ? const Color(0xff0a6836)
                                    : const Color(0xff9e9e9e)),
                            child: Center(
                              child: Text(
                                "Mua hàng",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: _checkbox
                                        ? const Color(0xfffffefa)
                                        : const Color(0xffebebeb)),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
