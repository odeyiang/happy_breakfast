import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';

class VoucherSrc extends StatefulWidget {
  const VoucherSrc({super.key});

  @override
  State<VoucherSrc> createState() => _VoucherSrcState();
}

enum SingingCharacter { one, two, three, four, five, six, seven }

class _VoucherSrcState extends State<VoucherSrc> {
  SingingCharacter? _character;
  TextEditingController search = TextEditingController();
  bool values = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Chọn voucher",
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            SizedBox(
              height: 30,
              child: Row(
                children: [
                  Expanded(
                    child: CupertinoSearchTextField(
                      style: const TextStyle(fontSize: 14),
                      controller: search,
                      decoration: BoxDecoration(
                          color: Colors.white70,
                          border: Border.all(width: 0.5, color: Colors.black54),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8))),
                      enabled: true,
                      placeholder: 'Nhập mã',
                      placeholderStyle:
                          const TextStyle(color: Colors.black54, fontSize: 12),
                      onTap: () {},
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 110,
                    decoration: BoxDecoration(
                        color: Colors.white70,
                        border: Border.all(width: 0.5, color: Colors.black54),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8))),
                    child: const Center(child: Text("Áp dụng")),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 7,
                  itemBuilder: (BuildContext context, int index) {
                    return Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 8),
                          height: 80,
                          decoration: const BoxDecoration(),
                          child: Row(
                            children: [
                              Container(
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                  image: AssetImage(
                                      "assets/images/voucher_bg.png"),
                                )),
                                child: Transform.scale(
                                    scale: 0.7,
                                    child: const Image(
                                      image: AssetImage(
                                          "assets/images/voucher_img.png"),
                                    )),
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: const BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                        color: Color(0xff858585),
                                        width: 0.2,
                                      ),
                                      top: BorderSide(
                                        color: Color(0xff858585),
                                        width: 0.2,
                                      ),
                                      bottom: BorderSide(
                                          color: Color(0xff858585), width: 0.2),
                                    ),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        bottomRight: Radius.circular(10))),
                                child: const Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Giảm 15% giảm tối đa 55k",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      "Đơn tối thiểu 300k",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      width: 200,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "HSD: 30/02/2023",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Color(0xff858585)),
                                          ),
                                          Text("Điều kiện",
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  color: Color(0xffb62c21)))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Radio<SingingCharacter>(
                          activeColor: const Color(0xff0a6836),
                          value: SingingCharacter.values[index],
                          groupValue: _character,
                          onChanged: (SingingCharacter? value) {
                            values = true;
                            setState(() {
                              _character = value!;
                            });
                          },
                        )
                      ],
                    );
                  }),
            ),
            SizedBox(
              height: 110,
              child: Column(
                children: [
                  const SizedBox(height: 10),
                  values
                      ? const Row(
                          children: [
                            Text("1 voucher được chọn",
                                style: TextStyle(
                                    color: Color(0xff858585),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 10)),
                            SizedBox(
                              width: 20,
                            ),
                            Text("Bạn đã được giảm 10k",
                                style: TextStyle(
                                    color: Color(0xff0a6836),
                                    fontWeight: FontWeight.w700,
                                    fontSize: 10))
                          ],
                        )
                      : Container(),
                  const SizedBox(height: 10),
                  CustomersButton(
                    borderColor: const Color(0xff0a6836),
                    buttonText: "Xác nhận",
                    width: MediaQuery.of(context).size.width,
                    bgcolor: const Color(0xff0a6836),
                    textColor: const Color(0xfffffefa),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
