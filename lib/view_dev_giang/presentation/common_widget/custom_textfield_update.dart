import 'package:flutter/material.dart';


class CustomTextFieldUpdate extends StatelessWidget {
  final double? width;
  final double? height;
  final String? hintText;
  final IconButton? icon;
  final String? text;
  final String? requiredText;
  final TextEditingController? controller;
  final String? errorText;
  final String? Function(String?)? validator;
  final void Function(String?)? onChanged;
  final Function(String)? onCompleted;

  const CustomTextFieldUpdate(
      {super.key,
        this.hintText,
        this.text,
        this.requiredText,
        this.controller,
        this.errorText,
        this.validator,
        this.onChanged,
        this.onCompleted, this.icon, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$text",
            style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto"),
          ),
          TextField(
            style: const TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontFamily: "Roboto"),
            controller: controller,
            decoration: InputDecoration(
              hintText: hintText,
              suffixIcon: icon,
              errorText: errorText
            ),
          ),
          const SizedBox(height: 10,)
        ],
      ),
    );
  }
}
