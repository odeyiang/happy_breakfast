import 'package:flutter/material.dart';

class CustomersButton extends StatelessWidget {
  final Function()? onPressed;
  final String buttonText;
  final EdgeInsets? margin;
  final double? height;
  final double width;
  final double? fontSize;
  final IconData? icon;
  final Color? bgcolor;
  final Color? textColor;
  final Color borderColor;

  const CustomersButton({
    Key? key,
    this.onPressed,
    required this.buttonText,
    this.margin,
    required this.width,
    this.height,
    this.fontSize,
    this.icon,
    this.bgcolor,
    this.textColor,
    required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          color: bgcolor,
          border: Border.all(color: borderColor),
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: TextButton(
        onPressed: onPressed,
        child: Text(
          buttonText,
          style: TextStyle(
              color: textColor,
              fontSize: fontSize,
              fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}
