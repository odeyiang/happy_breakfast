
import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';
import 'package:intl/intl.dart';


class CustomCardProduct extends StatelessWidget {
  final String? imagePath;
  final String text;
  const CustomCardProduct({super.key, this.imagePath, required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 0.5,color: Colors.grey),
        borderRadius: const BorderRadius.all(Radius.circular(8))
      ),
      //width: 160,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 180,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("$imagePath"), fit: BoxFit.fill)),
          ),

          Container(
            color: const Color(0xfffafafa),
            height: 100,
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  text,
                  maxLines: 5,
                  style: const TextStyle(
                      color: Color(0xff1f1f1f),
                      fontSize: 12,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: [
                    Text("${Const.convertPrice(400000)}đ",
                        style: const TextStyle(
                            decorationColor: Color(0xffa3a2a2),
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff0a6836))),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      "${Const.convertPrice(1000000)}đ",
                      style: const TextStyle(
                          decoration: TextDecoration.lineThrough,
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Color(0xffa3a2a2)),
                    )
                  ],
                ),
                RichText(
                  text: const TextSpan(children: [
                    TextSpan(
                        text: "Đã mua",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 10,
                            color: Color(0xff1f1f1f))),
                    TextSpan(
                        text: " 100",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 10,
                            color: Color(0xff1f1f1f)))
                  ]),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}


