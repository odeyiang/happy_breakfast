import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_card.dart';


class CustomCartProduct extends StatelessWidget {
  const CustomCartProduct({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 185,
      width: 343,
      decoration: const BoxDecoration(
        color: Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 64,
            width: 64,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/product.png"))),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const SizedBox(
                width: 250,
                child: Text(
                  "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE sgfasgfgasjhg",
                  style: TextStyle(fontSize: 14),
                ),
              ),
              const Text("SKU: 623148149"),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                height: 26,
                decoration: const BoxDecoration(
                    color: Color(0xff0a6838),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: const Center(
                    child: Text(
                  "Phân loại: Vị ổi",
                  style: TextStyle(
                      color: Color(0xfffafafa), fontWeight: FontWeight.w400),
                )),
              ),
              Row(
                children: [
                  Text("${Const.convertPrice(1000000)}đ",
                      style: const TextStyle(
                          decoration: TextDecoration.lineThrough,
                          decorationColor: Color(0xffa3a2a2),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xffa3a2a2))),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    "${Const.convertPrice(400000)}đ",
                    style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Color(0xff0a6838)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 88,
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 0.2, color: const Color(0xffD2D2D2))),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.remove_sharp,
                          size: 14,
                        ),
                        Text(
                          "1",
                          style: TextStyle(fontSize: 14),
                        ),
                        Icon(
                          Icons.add,
                          size: 14,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 120,
                  ),
                  const Icon(
                    Icons.delete_outline,
                    color: Color(0xffB62C21),
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
