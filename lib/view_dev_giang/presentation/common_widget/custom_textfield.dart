
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextInputField extends StatefulWidget {
  final bool readOnly;
  final bool enable;
  final bool showPassword;
  final bool obscureText;
  final TextEditingController? controller;
  final String? hint;
  final String? error;
  final Widget? leading;
  final Widget? trailing;
  final IconButton? icon;
  final FocusNode? focusNode;
  final String label;
  final TextInputType? textInputType;
  final BoxConstraints? prefixIconConstraint;
  final String? initialValue;
  final int? maxLength;
  final bool requiredLabel;
  final VoidCallback? onTap;
  final FormFieldSetter<String>? onChanged;
  final FormFieldSetter<String>? onFieldSubmitted;
  final FormFieldValidator<String>? validator;
  final List<TextInputFormatter>? inputFormatters;

  const AppTextInputField(
      {Key? key,
        this.readOnly = false,
        this.showPassword = false,
        this.obscureText = false,
        this.enable = true,
        this.controller,
        this.hint,
        this.leading,
        this.trailing,
        this.focusNode,
        required this.label,
        this.requiredLabel = false,
        this.error,
        this.textInputType,
        this.prefixIconConstraint,
        this.initialValue,
        this.maxLength,
        this.validator,
        this.onChanged,
        this.inputFormatters,
        this.onFieldSubmitted,
        this.onTap, this.icon})
      : super(key: key);

  @override
  State<AppTextInputField> createState() => _AppTextInputFieldState();
}

class _AppTextInputFieldState extends State<AppTextInputField> {
  late final FocusNode _focusNode;
  late bool _obscureText ;

  @override
  void initState() {
    super.initState();
    _focusNode = widget.focusNode ?? FocusNode();
    _obscureText = widget.obscureText;
    _focusNode.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    if (widget.focusNode == null) _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Color labelColor =
    !_focusNode.hasFocus ? const Color(0xFF727272) : const Color(0xff0a6838);
    if (widget.error != null) labelColor = const Color(0xFFF12C2C);
    return Column(
      children: [
        Container(
          color: Theme.of(context).scaffoldBackgroundColor,
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: [
              Text(
                widget.label,
                style: !_focusNode.hasFocus
                    ? Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(color: labelColor)
                    : Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(color: labelColor),
              ),
              widget.requiredLabel
                  ? const Text(
                "*",
                style: TextStyle(color: Colors.red),
              )
                  : const SizedBox()
            ],
          ),
        ),
        TextFormField(
          onTap: widget.onTap,
          obscureText: _obscureText,
          onChanged: widget.onChanged,
          onFieldSubmitted: widget.onFieldSubmitted,
          validator: widget.validator,
          controller: widget.controller,
          cursorColor: const Color(0xff0a6838),
          enabled: widget.enable,
          focusNode: _focusNode,
          initialValue: widget.initialValue,
          maxLengthEnforcement: MaxLengthEnforcement.enforced,
          maxLength: widget.maxLength,
          inputFormatters: widget.inputFormatters,
          style: Theme.of(context).textTheme.bodyMedium,
          buildCounter: (context,
              {required int currentLength,
                required bool isFocused,
                required int? maxLength}) {
            return const SizedBox();
          },
          keyboardType: widget.textInputType ?? TextInputType.text,
          onTapOutside: (event) {
            _focusNode.unfocus();
          },
          readOnly: widget.readOnly,
          decoration: InputDecoration(
            prefixIcon: widget.leading,
            prefixIconColor: Colors.black,
            prefixIconConstraints: widget.prefixIconConstraint,
            suffixIcon: widget.showPassword
                ? GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: Icon(
                    _obscureText ? Icons.visibility_off : Icons.visibility))
                : const SizedBox(),
            suffixIconColor: const Color(0xFFB9B9B9),
            hintText: widget.hint,
            hintStyle: Theme.of(context)
                .textTheme
                .bodyMedium
                ?.copyWith(color: const Color(0xFFB9B9B9)),
            errorText: widget.error,
            contentPadding: const EdgeInsets.all(12),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(color: Color(0xFFE8E8E8))),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(color: Color(0xff0a6838))),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(color: Color(0xFFE8E8E8))),
          ),
        ),
      ],
    );
  }
}
