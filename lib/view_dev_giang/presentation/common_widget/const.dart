import 'package:intl/intl.dart';

class Const {
  static final format = NumberFormat("#,##0.##", "vi");
  static bool isNumeric(String result) {
    return double.tryParse(result) != null;
  }

  static convertPrice(
      dynamic price,
      ) {
    var res = isNumeric(price.toString());
    if (res) {
      return format.format(double.parse(price.toString())).toString();
    }
    return "0";
  }
}