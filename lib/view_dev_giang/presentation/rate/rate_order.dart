import 'package:five_pointed_star/five_pointed_star.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';

class RateOrderSrc extends StatefulWidget {
  const RateOrderSrc({super.key});

  @override
  State<RateOrderSrc> createState() => _RateOrderSrcState();
}


final List<String> _titleStar = [
  'Tất cả',
  '5 sao',
  '4 sao',
  '3 sao',
  '2 sao',
  '1 sao'
];


class _RateOrderSrcState extends State<RateOrderSrc>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int mycount = 0;
  int? _value;
  int? ind;
  bool active = false;
  Color? color;
  final List<bool> _isSelected =
      List.generate(_titleStar.length, (index) => false);
  int initialIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _isSelected[initialIndex] = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Đánh giá của tôi",
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
        ),
      ),
      body: Column(
        children: [
          TabBar(
            isScrollable: true,
            indicatorColor: const Color(0xff0a6836),
            controller: _tabController,
            labelColor: const Color(0xff0a6836),
            indicatorSize: TabBarIndicatorSize.label,
            tabs: const [
              Tab(
                text: 'Chưa đánh giá',
              ),
              Tab(text: 'Đã đánh giá'),
              Tab(text: 'Đánh giá của người bán'),
            ],
          ),
          Expanded(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: TabBarView(controller: _tabController, children: [
                ListView.builder(
                    itemCount: 5,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.all(16),
                        padding: const EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            // border: Border.all(width: 0.1),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset("assets/images/store.svg"),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    const Text(
                                      "Nhà thuốc cổ truyền",
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ],
                                ),
                                const Text("Đã hoàn thành",
                                    style: TextStyle(
                                        fontSize: 12, color: Color(0xffE1655B)))
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey[300],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: 90,
                                    width: 60,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/product.png"),
                                            fit: BoxFit.fill)),
                                  ),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Text(
                                          "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        const SizedBox(
                                          width: 230,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("Phân loại: Vị ổi",
                                                  style: TextStyle(
                                                      color:
                                                          Color(0xff9e9e9e))),
                                              Text("x1",
                                                  style: TextStyle(
                                                      color: Color(0xff9e9e9e)))
                                            ],
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        SizedBox(
                                          width: 230,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              const Text(
                                                "Giá sản phẩm:",
                                              ),
                                              Text(
                                                "${Const.convertPrice(499000)}đ",
                                                style: const TextStyle(),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Divider(
                              height: 1,
                              color: Colors.grey[300],
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(horizontal: 8),
                              child: const Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Image(
                                      width: 24,
                                      image:
                                          AssetImage("assets/images/logo.png")),
                                  Text(
                                    "1 sản phẩm Thành tiền: ",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  Text("368.600đ",
                                      style: TextStyle(fontSize: 12))
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  flex: 7,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Đánh giá sản phẩm trước 26-02-2024",
                                        style: TextStyle(fontSize: 12),
                                      ),
                                      Text(
                                        "Đánh giá ngay và nhận 600 Điểm thưởng!",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xffDA3E31)),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 3,
                                  child: CustomersButton(
                                    buttonText: "Đánh giá",
                                    width: 174,
                                    height: 40,
                                    borderColor: const Color(0xff0a6836),
                                    bgcolor: const Color(0xff0a6836),
                                    textColor: const Color(0xfffffefa),
                                    onPressed: () {},
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                          ],
                        ),
                      );
                    }),
                ListView.builder(
                    itemCount: 5,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.all(16),
                        padding: const EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            // border: Border.all(width: 0.1),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 1,
                                child: Container(
                                  height: 40,
                                  width: 30,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/images/avatar2.png"),
                                          fit: BoxFit.cover)),
                                  child: Container(),
                                )),
                            Expanded(
                              flex: 5,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    "Nguyễn Tú Anh",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 12),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  FivePointedStar(
                                    size: const Size(15, 15),
                                    onChange: (count) {
                                      setState(() {
                                        mycount = count;
                                      });
                                    },
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const Row(
                                    children: [
                                      Text(
                                        "25-02-2024",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Color(0xff9e9e9e)),
                                      ),
                                      Text("15:06",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Color(0xff9e9e9e)))
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const Text("Chất lượng sản phẩm: Tốt",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Color(0xff1f1f1f))),
                                  const Text(
                                      "Uống ngon thải độc cho phổi rất tốt, thơm mùi ổi.",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Color(0xff1f1f1f))),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 40,
                                        width: 40,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8)),
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    "assets/images/product.png"),
                                                fit: BoxFit.fitHeight)),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(left: 10),
                                        child: const Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 11,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 230,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text("Phân loại: Vị ổi",
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Color(
                                                              0xff9e9e9e))),
                                                  Text("x1",
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          color: Color(
                                                              0xff9e9e9e)))
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              height: 8,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      SvgPicture.asset(
                                          "assets/images/edit_icon.svg"),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      const Text(
                                        "Sửa",
                                        style:
                                            TextStyle(color: Color(0xffda3e31)),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: const EdgeInsets.only(left: 8),
                              width: 100,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Text(
                                    "0.0",
                                    style: TextStyle(
                                        fontSize: 32,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  FivePointedStar(
                                    size: const Size(15, 15),
                                    onChange: (count) {
                                      setState(() {
                                        mycount = count;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              height: 100,
                              child: GridView.builder(
                                shrinkWrap: true,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        mainAxisSpacing: 4,
                                        crossAxisSpacing: 4,
                                        childAspectRatio: 2),
                                itemCount: _titleStar.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _isSelected[initialIndex] = false;
                                        _isSelected[index] = true;
                                        initialIndex = index;
                                      });
                                    },
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 4),
                                      decoration: BoxDecoration(
                                          color: const Color(0xffebebeb),
                                          border: Border.all(
                                              color: _isSelected[index]
                                                  ? const Color(0xff0a6836)
                                                  : const Color(0xffebebeb)),
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(8))),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: _isSelected[index]
                                              ? Colors.white
                                              : const Color(0xffebebeb),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "${_titleStar[index]}(10)",
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: _isSelected[index]
                                                    ? const Color(0xff0a6836)
                                                    : const Color(0xff1f1f1f)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height,
                          color: const Color(0xfff5f5f5),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset("assets/images/not_value.svg"),
                                const SizedBox(
                                  height: 20,
                                ),
                                const Text(
                                    "Bạn chưa nhận được đánh giá từ người bán")
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
