import 'package:five_pointed_star/five_pointed_star.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/cart/cart.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/profile/widget/custom_notification.dart';

class RateDetailProductSrc extends StatefulWidget {
  const RateDetailProductSrc({super.key});

  @override
  State<RateDetailProductSrc> createState() => _RateDetailProductSrcState();
}

enum SingChoose { all, imageVideo, star }

enum RadioChoose { five, four, three, two, one }

class RadioChooseItem extends StatefulWidget {
  const RadioChooseItem({super.key});

  @override
  State<RadioChooseItem> createState() => _RadioChooseItemState();
}

class _RadioChooseItemState extends State<RadioChooseItem> {
  RadioChoose? radioChoose;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 350,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Radio<RadioChoose>(
                  value: RadioChoose.five,
                  activeColor: const Color(0xff0a6836),
                  groupValue: radioChoose,
                  onChanged: (RadioChoose? value) {
                    setState(() {
                      radioChoose = value!;
                    });
                  },
                ),
                FivePointedStar(
                  selectedColor: Colors.orangeAccent,
                  size: const Size(15, 15),
                  defaultSelectedCount: 5,
                  count: 5,
                  disabled: true,
                ),
                const Text("20")
              ],
            ),
            Row(
              children: [
                Radio<RadioChoose>(
                  value: RadioChoose.four,
                  activeColor: const Color(0xff0a6836),
                  groupValue: radioChoose,
                  onChanged: (RadioChoose? value) {
                    setState(() {
                      radioChoose = value!;
                    });
                  },
                ),
                FivePointedStar(
                  selectedColor: Colors.orangeAccent,
                  size: const Size(15, 15),
                  defaultSelectedCount: 4,
                  count: 4,
                  disabled: true,
                ),
                const Text("20")
              ],
            ),
            Row(
              children: [
                Radio<RadioChoose>(
                  value: RadioChoose.three,
                  activeColor: const Color(0xff0a6836),
                  groupValue: radioChoose,
                  onChanged: (value) {
                    setState(() {
                      radioChoose = value;
                    });
                  },
                ),
                FivePointedStar(
                  selectedColor: Colors.orangeAccent,
                  size: const Size(15, 15),
                  defaultSelectedCount: 3,
                  count: 3,
                  disabled: true,
                ),
                const Text("20")
              ],
            ),
            Row(
              children: [
                Radio<RadioChoose>(
                  value: RadioChoose.two,
                  activeColor: const Color(0xff0a6836),
                  groupValue: radioChoose,
                  onChanged: (value) {
                    setState(() {
                      radioChoose = value;
                    });
                  },
                ),
                FivePointedStar(
                  selectedColor: Colors.orangeAccent,
                  size: const Size(15, 15),
                  defaultSelectedCount: 2,
                  count: 2,
                  disabled: true,
                ),
                const Text("20")
              ],
            ),
            Row(
              children: [
                Radio<RadioChoose>(
                  value: RadioChoose.one,
                  activeColor: const Color(0xff0a6836),
                  groupValue: radioChoose,
                  onChanged: (value) {
                    setState(() {
                      radioChoose = value;
                    });
                  },
                ),
                FivePointedStar(
                  selectedColor: Colors.orangeAccent,
                  size: const Size(15, 15),
                  defaultSelectedCount: 1,
                  count: 1,
                  disabled: true,
                ),
                const Text("20")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: const CustomersButton(
                    buttonText: "Bỏ lọc",
                    width: 166,
                    height: 40,
                    borderColor: Color(0xff0a6836),
                    textColor: Color(0xff0a6836),
                    bgcolor: Colors.white,
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: const CustomersButton(
                    buttonText: "Đồng ý",
                    width: 166,
                    height: 40,
                    borderColor: Color(0xff0a6836),
                    textColor: Color(0xfffffefa),
                    bgcolor: Color(0xff0a6836),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _RateDetailProductSrcState extends State<RateDetailProductSrc> {
  SingChoose? singChoose;
  int myStar = 0;
  int count = 10;
  bool onLike = false;

  List<String> asset = [
    "assets/images/avatar.png",
    "assets/images/product.png",
    "assets/images/product1.png",
    "assets/images/product2.png",
  ];
  List<bool> itemLikes = [false, false, false, false];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Đánh giá",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const CartSrc()));
            },
            child: const CustomNotification(
                icon: Icons.shopping_cart_outlined, textCount: "99+"),
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        singChoose = SingChoose.all;
                      });
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4)),
                          color: Colors.grey[100],
                          border: Border.all(
                              color: singChoose == SingChoose.all
                                  ? const Color(0xff0a6836)
                                  : const Color(0xff919191))),
                      child: const Center(child: Text("Tất cả(100)")),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        singChoose = SingChoose.imageVideo;
                      });
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4)),
                          border: Border.all(
                              color: singChoose == SingChoose.imageVideo
                                  ? const Color(0xff0a6836)
                                  : const Color(0xff919191))),
                      child: const Center(
                          child: Text(
                        "Có hình ảnh, video(100)",
                        textAlign: TextAlign.center,
                      )),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        singChoose = SingChoose.star;
                      });
                      showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return const RadioChooseItem();
                        },
                      );
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4)),
                          color: Colors.grey[100],
                          border: Border.all(
                              color: singChoose == SingChoose.star
                                  ? const Color(0xff0a6836)
                                  : const Color(0xff919191))),
                      child: Center(
                          child: RichText(
                        text: const TextSpan(children: [
                          TextSpan(
                              text: "Sao",
                              style: TextStyle(color: Color(0xff1f1f1f))),
                          WidgetSpan(
                              child: Icon(
                            Icons.star_rate,
                            color: Colors.orangeAccent,
                            size: 20,
                          )),
                          TextSpan(
                              text: "(100)",
                              style: TextStyle(color: Color(0xff1f1f1f)))
                        ]),
                      )),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12),
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: const BorderRadius.all(Radius.circular(8))
                        //border: Border.all()
                        ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 44,
                                  width: 44,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/images/avatar2.png"),
                                          fit: BoxFit.cover)),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text(
                                      "Tú Anh",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700),
                                    ),
                                    FivePointedStar(
                                      selectedColor: Colors.orangeAccent,
                                      size: const Size(15, 15),
                                      defaultSelectedCount: 4,
                                      disabled: true,
                                      //  onChange: (count) {
                                      // setState(() {
                                      //   myStar = count;
                                      // });
                                      // },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                IconButton(
                                    onPressed: () {
                                      setState(() {
                                        itemLikes[index] = !itemLikes[index];
                                      });
                                    },
                                    icon: Icon(
                                      Icons.thumb_up,
                                      color: itemLikes[index]
                                          ? Colors.blue
                                          : Colors.grey,
                                    )),
                                Text(itemLikes[index]
                                    ? "${count + 1}"
                                    : "$count"),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          "Đây là một sản phẩm tuyệt vời rất đáng để trải nghiệm, nó giúp tôi cảm thất rất thoải mái khi sử dụng,...",
                          style: TextStyle(fontSize: 12),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        SizedBox(
                          height: 70,
                          child: ListView.separated(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, position) {
                              return Container(
                                height: 60,
                                width: 75,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(asset[position]),
                                        fit: BoxFit.fill)),
                              );
                            },
                            separatorBuilder: (context, position) {
                              return const SizedBox(
                                width: 10,
                              );
                            },
                            itemCount: asset.length,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text("July,23 2024")
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 16,
                  );
                },
                itemCount: 4,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
