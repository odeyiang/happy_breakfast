import 'dart:io';

import 'package:five_pointed_star/five_pointed_star.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:image_picker/image_picker.dart';

class CreateRateSrc extends StatefulWidget {
  const CreateRateSrc({super.key});

  @override
  State<CreateRateSrc> createState() => _CreateRateSrcState();
}

class _CreateRateSrcState extends State<CreateRateSrc> {
  int myCount = 0;
  int myRate = 0;
  int myShip = 0;
  bool show = false;
  bool active = false;
  XFile? image;
  XFile? video;
  String? levelStar;
  TextEditingController quality = TextEditingController();
  TextEditingController textEvaluate = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Đánh giá của tôi",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    border: Border.all(width: 0.1),
                    borderRadius: const BorderRadius.all(Radius.circular(8))),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Expanded(
                          flex: 3,
                          child: Text(
                            "Chất lượng sản phẩm",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: FivePointedStar(
                            size: const Size(20, 20),
                            onChange: (count) {
                              setState(() {
                                active = true;
                                myCount = count;
                              });
                            },
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: Center(
                              child: Text(
                                myCount == 0
                                    ? "Chưa đánh giá"
                                    : myCount == 1
                                        ? "Tệ"
                                        : myCount == 2
                                            ? "Không hài lòng"
                                            : myCount == 3
                                                ? "Bình thường"
                                                : myCount == 4
                                                    ? "Hài lòng"
                                                    : "Tuyệt vời",
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: Color(0xffffb41f),
                                ),
                              ),
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    RichText(
                        text: const TextSpan(children: [
                      TextSpan(
                          text:
                              "Thêm 60 ký tự, 1 hình ảnh và 1 video để nhận đến ",
                          style: TextStyle(color: Color(0xff9e9e9e))),
                      TextSpan(
                          text: "600 điểm thưởng",
                          style: TextStyle(color: Color(0xffb62c21)))
                    ]))
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () async {
                      final picker = ImagePicker();
                      final img =
                          await picker.pickImage(source: ImageSource.gallery);
                      if (img != null) {
                        setState(() {
                          image = img;
                        });
                      }
                    },
                    child: Column(
                      children: [
                        Container(
                          width: 160,
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              border:
                                  Border.all(color: const Color(0xff0a6836))),
                          child: Column(
                            children: [
                              SvgPicture.asset("assets/images/camera.svg"),
                              const Text("Thêm Hình ảnh",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Color(0xff0a6836),
                                  )),
                            ],
                          ),
                        ),
                        if (image != null)
                          SizedBox(
                            height: 60,
                            width: 160,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    height: 50,
                                    decoration: const BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    child: Image.file(
                                      File(image!.path),
                                      //fit: BoxFit.contain,
                                    )),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: const EdgeInsets.all(14),
                                    decoration: const BoxDecoration(
                                      color: Color(0xffe6e8ec),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    child: SvgPicture.asset(
                                      "assets/images/add_image.svg",
                                    ))
                              ],
                            ),
                          )
                        else
                          const SizedBox(),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      final picker = ImagePicker();
                      final vid =
                          await picker.pickVideo(source: ImageSource.gallery);
                      setState(() {
                        video = vid;
                      });
                    },
                    child: Column(
                      children: [
                        Container(
                          width: 160,
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8)),
                              border:
                                  Border.all(color: const Color(0xff0a6836))),
                          child: Column(
                            children: [
                              SvgPicture.asset("assets/images/video.svg"),
                              const Text("Thêm Video",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Color(0xff0a6836),
                                  ))
                            ],
                          ),
                        ),
                        if (video != null)
                          SizedBox(
                            height: 60,
                            width: 160,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                    height: 50,
                                    child: Image.file(
                                      File(video!.path),
                                    )),
                                Container(
                                    height: 50,
                                    width: 50,
                                    padding: const EdgeInsets.all(14),
                                    decoration: const BoxDecoration(
                                      color: Color(0xffe6e8ec),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    child: SvgPicture.asset(
                                      "assets/images/add_video.svg",
                                    ))
                              ],
                            ),
                          )
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                decoration: BoxDecoration(
                    border: Border.all(width: 0.1),
                    borderRadius: const BorderRadius.all(Radius.circular(8))),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Text("Chất lượng sản phẩm: "),
                        Expanded(
                          child: TextField(
                            controller: quality,
                            decoration: const InputDecoration.collapsed(
                                hintText: "để lại đánh giá của bạn",
                                hintStyle: TextStyle(
                                  fontSize: 12,
                                )),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 5),
                    const Divider(
                      height: 1,
                      color: Colors.grey,
                    ),
                    const SizedBox(height: 10),
                    TextField(
                      maxLines: 8,
                      controller: textEvaluate,
                      decoration: const InputDecoration.collapsed(
                        hintText:
                            "Hãy chia sẻ nhận xét cho sản phẩm này bạn nhé!",
                        hintStyle: TextStyle(fontSize: 16),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                decoration: BoxDecoration(
                    border: Border.all(width: 0.1),
                    borderRadius: const BorderRadius.all(Radius.circular(8))),
                child: Column(
                  children: [
                    Row(children: [
                      const Expanded(
                        flex: 4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Hiển thị tên đăng nhập trên đánh giá này",
                              style: TextStyle(fontSize: 12),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                                "Tên tài khoản của bạn sẻ hiển thị như Nguyễn Tú Anh",
                                style:
                                    TextStyle(fontSize: 10, color: Colors.grey))
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Switch(
                            activeColor: const Color(0xff26a44d),
                            value: show,
                            inactiveThumbColor: Colors.white,
                            onChanged: (bool value) {
                              setState(() {
                                show = value;
                              });
                            }),
                      )
                    ]),
                    const Divider(height: 1),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 3,
                            child: Text(
                              "Dịch vụ của người bán",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: FivePointedStar(
                              size: const Size(20, 20),
                              onChange: (count) {
                                setState(() {
                                  myRate = count;
                                });
                              },
                            ),
                          ),
                          Expanded(
                              flex: 2,
                              child: Center(
                                child: Text(
                                  myRate == 0
                                      ? "Chưa đánh giá"
                                      : myRate == 1
                                          ? "Tệ"
                                          : myRate == 2
                                              ? "Không hài lòng"
                                              : myRate == 3
                                                  ? "Bình thường"
                                                  : myRate == 4
                                                      ? "Hài lòng"
                                                      : "Tuyệt vời",
                                  style: const TextStyle(
                                    fontSize: 16,
                                    color: Color(0xffffb41f),
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                    const Divider(
                      height: 1,
                      color: Colors.grey,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 3,
                            child: Text(
                              "Dịch vụ vận chuyển",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: FivePointedStar(
                              size: const Size(20, 20),
                              onChange: (count) {
                                setState(() {
                                  myShip = count;
                                });
                              },
                            ),
                          ),
                          Expanded(
                              flex: 2,
                              child: Center(
                                child: Text(
                                  myShip == 0
                                      ? "Chưa đánh giá"
                                      : myShip == 1
                                          ? "Tệ"
                                          : myShip == 2
                                              ? "Không hài lòng"
                                              : myShip == 3
                                                  ? "Bình thường"
                                                  : myShip == 4
                                                      ? "Hài lòng"
                                                      : "Tuyệt vời",
                                  style: const TextStyle(
                                    fontSize: 16,
                                    color: Color(0xffffb41f),
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              GestureDetector(
                onTap: () {},
                child: CustomersButton(
                    buttonText: "Gửi",
                    width: MediaQuery.of(context).size.width,
                    bgcolor: active
                        ? const Color(0xff0a6836)
                        : const Color(0xffc4c4c4),
                    borderColor: active
                        ? const Color(0xff0a6836)
                        : const Color(0xffc4c4c4),
                    textColor: active
                        ? const Color(0xfffffefe)
                        : const Color(0xff919696)),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
