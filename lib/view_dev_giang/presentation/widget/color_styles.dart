

import 'package:flutter/material.dart';

class ColorAppStyle {

  static const Color greyD8 = Color(0xffd8d6de);
  static const Color button = Color(0xff0A6836);
  static const Color textButton = Color(0xffFFFEFA);
}

const Color primaryColor = Color(0xff0A6836);
