import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_card.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_infomation_detail.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_tracking.dart';

import '../common_widget/const.dart';

class OrderOptionSrc extends StatefulWidget {
  const OrderOptionSrc({super.key});

  @override
  State<OrderOptionSrc> createState() => _OrderOptionSrcState();
}

class _OrderOptionSrcState extends State<OrderOptionSrc>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 5, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Đơn hàng',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TabBar(
              isScrollable: true,
              indicatorColor: const Color(0xff0a6836),
              controller: _tabController,
              labelColor: const Color(0xff0a6836),
              indicatorSize: TabBarIndicatorSize.label,
              tabs: const [
                Tab(
                  text: 'Chờ xác nhận',
                ),
                Tab(text: 'Chờ lấy hàng'),
                Tab(text: 'Đang giao hàng'),
                Tab(
                  text: "Hoàn thành",
                ),
                SizedBox(
                    width: 50,
                    child: Tab(
                      text: "Huỷ",
                    ))
              ],
            ),
            SizedBox(
              height: 500,
              child: TabBarView(
                controller: _tabController,
                children: [
                  ListView.builder(
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.all(16),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.1),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8))),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      height: 74,
                                      width: 74,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/product.png"),
                                      )),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/shopping_bag.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              const Text("1 hộp",
                                                  style: TextStyle(
                                                      color: Color(0xff525252)))
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/monetization_on.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                "${Const.convertPrice(499000)}đ",
                                                style: const TextStyle(
                                                    color: Color(0xff525252)),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Xem chi tiết",
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xff9e9e9e)),
                                      ))
                                ],
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "6 sản phẩm",
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Color(0xff9e9e9e)),
                                        )),
                                    Expanded(
                                      flex: 5,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Thành tiền",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          Text("999.999đ",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xff0a6836)))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: CustomersButton(
                                  buttonText: "Theo dõi đơn hàng",
                                  width: 174,
                                  height: 40,
                                  borderColor: const Color(0xff0a6836),
                                  bgcolor: const Color(0xff0a6836),
                                  textColor: const Color(0xfffffefa),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const OrderTrackingSrc()));
                                  },
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                        );
                      }),
                  ListView.builder(
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.all(16),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.1),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8))),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      height: 74,
                                      width: 74,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/product.png"))),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/shopping_bag.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              const Text("1 hộp",
                                                  style: TextStyle(
                                                      color: Color(0xff525252)))
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/monetization_on.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                "${Const.convertPrice(499000)}đ",
                                                style: const TextStyle(
                                                    color: Color(0xff525252)),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Xem chi tiết",
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xff9e9e9e)),
                                      ))
                                ],
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "6 sản phẩm",
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Color(0xff9e9e9e)),
                                        )),
                                    Expanded(
                                      flex: 5,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Thành tiền",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          Text("999.999đ",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xff0a6836)))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: CustomersButton(
                                  buttonText: "Liên hệ shop",
                                  width: 174,
                                  height: 40,
                                  borderColor: const Color(0xff0a6836),
                                  bgcolor: const Color(0xff0a6836),
                                  textColor: const Color(0xfffffefa),
                                  onPressed: () {},
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                        );
                      }),
                  ListView.builder(
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.all(16),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.1),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8))),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      height: 74,
                                      width: 74,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/product.png"))),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/shopping_bag.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              const Text("1 hộp",
                                                  style: TextStyle(
                                                      color: Color(0xff525252)))
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/monetization_on.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                "${Const.convertPrice(499000)}đ",
                                                style: const TextStyle(
                                                    color: Color(0xff525252)),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Xem chi tiết",
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xff9e9e9e)),
                                      ))
                                ],
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "6 sản phẩm",
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Color(0xff9e9e9e)),
                                        )),
                                    Expanded(
                                      flex: 5,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Thành tiền",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          Text("999.999đ",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xff0a6836)))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: CustomersButton(
                                  buttonText: "Liên hệ shop",
                                  width: 174,
                                  height: 40,
                                  borderColor: const Color(0xff0a6836),
                                  bgcolor: const Color(0xff0a6836),
                                  textColor: const Color(0xfffffefa),
                                  onPressed: () {},
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                        );
                      }),
                  ListView.builder(
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    const OrderInformationDetailSrc()));
                          },
                          child: Container(
                            margin: const EdgeInsets.all(16),
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                border: Border.all(width: 0.1),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(8))),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        height: 74,
                                        width: 74,
                                        decoration: const BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    "assets/images/product.png"))),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Container(
                                        margin: const EdgeInsets.only(left: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text(
                                              "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                SvgPicture.asset(
                                                    "assets/images/shopping_bag.svg"),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                const Text("1 hộp",
                                                    style: TextStyle(
                                                        color: Color(0xff525252)))
                                              ],
                                            ),
                                            const SizedBox(
                                              height: 8,
                                            ),
                                            Row(
                                              children: [
                                                SvgPicture.asset(
                                                    "assets/images/monetization_on.svg"),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  "${Const.convertPrice(499000)}đ",
                                                  style: const TextStyle(
                                                      color: Color(0xff525252)),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    const Expanded(
                                        flex: 2,
                                        child: Text(
                                          "Xem chi tiết",
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Color(0xff9e9e9e)),
                                        ))
                                  ],
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                Container(
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: const Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text(
                                            "6 sản phẩm",
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Color(0xff9e9e9e)),
                                          )),
                                      Expanded(
                                        flex: 5,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Thành tiền",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            Text("999.999đ",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xff0a6836)))
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Divider(
                                  height: 1,
                                  color: Colors.grey[300],
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    CustomersButton(
                                      buttonText: "Đánh giá",
                                      width: 164,
                                      height: 40,
                                      borderColor: const Color(0xff0a6836),
                                      textColor: const Color(0xff0a6836),
                                      onPressed: () {},
                                    ),
                                    CustomersButton(
                                      buttonText: "Mua lại",
                                      width: 164,
                                      height: 40,
                                      borderColor: const Color(0xff0a6836),
                                      bgcolor: const Color(0xff0a6836),
                                      textColor: const Color(0xfffffefa),
                                      onPressed: () {},
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                  ListView.builder(
                      itemCount: 2,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.all(16),
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.1),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(8))),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      height: 74,
                                      width: 74,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/product.png"))),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Text(
                                            "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/shopping_bag.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              const Text("1 hộp",
                                                  style: TextStyle(
                                                      color: Color(0xff525252)))
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              SvgPicture.asset(
                                                  "assets/images/monetization_on.svg"),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                "${Const.convertPrice(499000)}đ",
                                                style: const TextStyle(
                                                    color: Color(0xff525252)),
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Xem chi tiết",
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xff9e9e9e)),
                                      ))
                                ],
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "6 sản phẩm",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color(0xff9e9e9e)),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(
                                height: 1,
                                color: Colors.grey[300],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: CustomersButton(
                                  buttonText: "Mua lại",
                                  width: 164,
                                  height: 40,
                                  borderColor: const Color(0xff0a6836),
                                  bgcolor: const Color(0xff0a6836),
                                  textColor: const Color(0xfffffefa),
                                  onPressed: () {},
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                        );
                      }),
                ],
              ),
            ),
            Container(
              height: 400,
              margin: const EdgeInsets.all(16),
              child: Column(
                children: [
                  const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Gợi ý cho bạn",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w700),
                      )),
                  const SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: GridView.builder(
                      primary: false,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 0.65,
                        crossAxisSpacing: 16,
                        mainAxisSpacing: 16,
                      ),
                      shrinkWrap: true,
                      itemCount: 4,
                      itemBuilder: (context, index) {
                        return const CustomCardProduct(
                          text:
                              'Gói 10 bữa sáng dinh dưỡng và thanh lọc (10 packs)',
                          imagePath: "assets/images/product1.png",
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
