import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';

class OrderTrackingSrc extends StatefulWidget {
  const OrderTrackingSrc({super.key});

  @override
  State<OrderTrackingSrc> createState() => _OrderTrackingSrcState();
}

class _OrderTrackingSrcState extends State<OrderTrackingSrc> {
  int count = 1;

  void increase() {
    setState(() {
      count == 4 ? count = 1 : count++;
    });
  }

  List<String> content = [
    "Đặt hàng thành công",
    "Đơn hàng đang được chuẩn bị",
    "Đơn hàng đã được bàn giao cho đơn vị vận chuyển",
    "Đơn hàng đang trên đường giao đến bạn"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Theo dõi đơn hàng $count",
          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
        actions: [
          IconButton(onPressed: () => increase(), icon: const Icon(Icons.add))
        ],
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 140,
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  border: Border.all(width: 0.1),
                  borderRadius: const BorderRadius.all(Radius.circular(8))),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Container(
                          height: 80,
                          decoration: const BoxDecoration(
                              color: Colors.green,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/product.png"),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "ID: 123456789",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Row(
                                children: [
                                  SvgPicture.asset(
                                      "assets/images/shopping_bag.svg"),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  const Text("4 sản phẩm",
                                      style:
                                          TextStyle(color: Color(0xff525252)))
                                ],
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  SvgPicture.asset(
                                      "assets/images/monetization_on.svg"),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "đ${Const.convertPrice(219000)}",
                                    style: const TextStyle(
                                        color: Color(0xff525252)),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Expanded(
                          flex: 3,
                          child: Text(
                            "26/09/2023",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12, color: Color(0xff9e9e9e)),
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 8),
                    child: const Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Vận chuyển bởi GHN-SYD LM",
                          style:
                              TextStyle(fontSize: 12, color: Color(0xff919696)),
                        )),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 100 * count.toDouble(),
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  border: Border.all(width: 0.1),
                  borderRadius: const BorderRadius.all(Radius.circular(8))),
              child: ListView.builder(
                physics:const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                reverse: true,
                itemCount: count,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 80,
                    child: Row(
                      children: [
                        const Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [Text("26 Tháng 1"), Text("7:00")],
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 30,
                          width: 30,
                          decoration: const BoxDecoration(
                            color: Colors.green,
                            shape: BoxShape.circle,
                          ),
                          child: const Center(
                            child: Icon(
                              Icons.note_add_rounded,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Flexible(
                            child: Text(
                                content[index]))
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
