import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/const.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/order_options/order_infomation_detail.dart';


class OrderInformation extends StatefulWidget {
  const OrderInformation({super.key});

  @override
  State<OrderInformation> createState() => _OrderInformationState();
}

class _OrderInformationState extends State<OrderInformation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Thông tin đơn hàng",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const OrderInformationDetailSrc()));
              },
              child: const Text("Chi tiết",style: TextStyle(color: Color(0xff0a6836)),))
        ],
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 32,
          ),
          GestureDetector(
            onTap: (){
              Navigator.of(context).push(_createRoute());
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              padding: const EdgeInsets.all(16),
              decoration: const BoxDecoration(
                color: Color(0xff0a6836),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Giao hàng nhanh",
                        style: TextStyle(
                            color: Color(0xfffafafa),
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        "Mã vận đơn: VN123456789",
                        style: TextStyle(
                            color: Color(0xfffafafa),
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                  SvgPicture.asset(
                    "assets/images/truck.svg",
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(color: Colors.grey[100]),
            child: Column(
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      "assets/images/truck2.svg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text(
                      "Thông tin vận chuyển",
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    )
                  ],
                ),
                const Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.fiber_manual_record,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Đang giao hàng"),
                              Text("02/02/2024 14:43")
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    )
                  ],
                ),
                const Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.fiber_manual_record,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Đã lấy hàng"),
                              Text("02/02/2024 14:43")
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    )
                  ],
                ),
                const Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.fiber_manual_record,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Đang chuẩn bị hàng"),
                              Text("02/02/2024 14:43")
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    )
                  ],
                ),
                const Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.fiber_manual_record,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Thông tin đơn hàng đã gửi tới shop"),
                              Text("02/02/2024 14:43")
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    )
                  ],
                ),
                const Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.fiber_manual_record,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Đã tạo đơn hàng"),
                              Text("02/02/2024 14:43")
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Route _createRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        const OverlayScreen(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = const Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: Scaffold(
            body:
                Padding(padding: const EdgeInsets.only(top: 60), child: child)),
      );
    },
  );
}

class OverlayScreen extends StatelessWidget {
  const OverlayScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(16),
                    decoration: const BoxDecoration(
                      color: Color(0xff0a6836),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Shop đã xác nhận đơn hàng",
                                style: TextStyle(
                                    color: Color(0xfffafafa),
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "Đơn hàng sẽ được giao cho đơn vị cận chuyển trước 02/02/2024",
                                style: TextStyle(
                                    color: Color(0xfffafafa),
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ),
                        SvgPicture.asset(
                          "assets/images/truck.svg",
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.1, color: Colors.black12)),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    SvgPicture.asset(
                                      "assets/images/truck2.svg",
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    const Text(
                                      "Thông tin vận chuyển",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    )
                                  ],
                                ),
                                const Text("Giao hàng nhanh - VN123456789")
                              ],
                            ),
                            const Text("Xem"),
                          ],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/images/map-pin-alt.svg",
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            const Text(
                              "Địa chỉ nhận hàng",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700, fontSize: 14),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text("Nguyễn Tú Anh"),
                        const Text("(+84) 982719167"),
                        const Text(
                            "Số 84, ngõ 66A Triều Khúc, Tân Triều, Thanh Trì, Hà Nội"),
                        const SizedBox(
                          height: 5,
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.1, color: Colors.black12)),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 80,
                                decoration: const BoxDecoration(
                                    color: Colors.green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/product.png"),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: const Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 12,
                                    ),
                                    Text("x1",
                                        style: TextStyle(
                                            color: Color(0xff525252))),
                                    SizedBox(
                                      height: 8,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text("${Const.convertPrice(1000000)}đ",
                                style: const TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    decorationColor: Color(0xffa3a2a2),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xffa3a2a2))),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${Const.convertPrice(400000)}đ",
                              style: const TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff0a6836)),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  width: 10,
                                ),
                                const Text(
                                  "Thành tiền",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 14),
                                ),
                                RichText(
                                  text: const TextSpan(children: [
                                    TextSpan(
                                      text: "Vui lòng thanh toán ",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Color(0xff1f1f1f)),
                                    ),
                                    TextSpan(
                                      text: "400.000đ ",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Color(0xff0a6836)),
                                    ),
                                    TextSpan(
                                      text: "khi nhận hàng",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Color(0xff1f1f1f)),
                                    )
                                  ]),
                                )
                              ],
                            ),
                            Text(
                              "${Const.convertPrice(400000)}đ",
                              style: const TextStyle(
                                  color: Color(0xff0a6836),
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.1, color: Colors.black12)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/images/circle-dollar.svg",
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            const Text(
                              "Phương thức thanh toán",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700, fontSize: 14),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        const Text("Thanh toán khi nhận hàng")
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    height: 250,
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.1, color: Colors.black12)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(
                              'assets/images/memo.svg',
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            const Text(
                              "Thông tin thanh toán",
                              style: TextStyle(fontWeight: FontWeight.w700),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Tổng tiền hàng",
                                style: TextStyle(fontWeight: FontWeight.w400)),
                            Text("${Const.convertPrice(800000)}đ")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Tổng tiền vận chuyển",
                                style: TextStyle(fontWeight: FontWeight.w400)),
                            Text("+${Const.convertPrice(22000)}đ")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Mã giảm giá",
                                style: TextStyle(fontWeight: FontWeight.w400)),
                            Text("-${Const.convertPrice(50000)}đ")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Tổng thanh toán",
                                style: TextStyle(fontWeight: FontWeight.w500)),
                            Text(
                              "${Const.convertPrice(772000)}đ",
                              style: const TextStyle(
                                  color: Color(0xff0a6836),
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
