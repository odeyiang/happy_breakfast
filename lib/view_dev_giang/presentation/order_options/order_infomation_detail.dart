import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';

import '../common_widget/const.dart';

class OrderInformationDetailSrc extends StatefulWidget {
  const OrderInformationDetailSrc({super.key});

  @override
  State<OrderInformationDetailSrc> createState() =>
      _OrderInformationDetailSrcState();
}

class _OrderInformationDetailSrcState extends State<OrderInformationDetailSrc> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Chi tiết đơn hàng",
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.all(24),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Chi tiết đơn hàng",
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
              const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Ngày đặt hàng",
                    style: TextStyle(fontSize: 12, color: Color(0xff9e9e9e)),
                  ),
                  Text("26/09/2023",
                      style: TextStyle(fontSize: 12, color: Color(0xff9e9e9e)))
                ],
              ),
              const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Mã đơn hàng",
                      style: TextStyle(fontSize: 12, color: Color(0xff9e9e9e))),
                  Text("123456789",
                      style: TextStyle(fontSize: 12, color: Color(0xff9e9e9e)))
                ],
              ),
              const SizedBox(
                height: 12,
              ),
              Divider(
                height: 1,
                color: Colors.grey[300],
              ),
              SizedBox(
                height: 460,
                child: ListView.builder(
                    itemCount: 4,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.only(top: 24),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 90,
                                width: 74,
                                decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/product.png"),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              flex: 4,
                              child: Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "Tinh chất Detox Phổi (Hộp 14 gói) - VITRUE",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        SvgPicture.asset(
                                            "assets/images/shopping_bag.svg"),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        const Text("1 hộp",
                                            style: TextStyle(
                                                color: Color(0xff525252)))
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        SvgPicture.asset(
                                            "assets/images/monetization_on.svg"),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "${Const.convertPrice(499000)}đ",
                                          style: const TextStyle(
                                              color: Color(0xff525252)),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              ),
              Divider(
                height: 1,
                color: Colors.grey[300],
              ),
              const Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "2 sản phẩm",
                  style: TextStyle(fontSize: 12, color: Color(0xff9e9e9e)),
                ),
              ),
              Container(
                height: 250,
                margin: const EdgeInsets.all(0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Tổng tiền hàng",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("${Const.convertPrice(800000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Tổng tiền vận chuyển",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("+${Const.convertPrice(22000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Mã giảm giá",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("-${Const.convertPrice(50000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Thành tiền",
                            style: TextStyle(fontWeight: FontWeight.w600)),
                        Text(
                          "${Const.convertPrice(772000)}đ",
                          style: const TextStyle(
                              color: Color(0xffB62C21),
                              fontWeight: FontWeight.w700,
                              fontSize: 18),
                        )
                      ],
                    ),
                    CustomersButton(
                      borderColor: const Color(0xff0a6836),
                      textColor: const Color(0xfffffefa),
                      bgcolor: const Color(0xff0a6836),
                      buttonText: 'Đánh giá',
                      width: MediaQuery.of(context).size.width,
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
