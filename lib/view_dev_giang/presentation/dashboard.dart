import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/profile/profile.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/profile/update_profile.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/widget/color_styles.dart';

class DashboardSrc extends StatefulWidget {
  const DashboardSrc({super.key});

  @override
  State<DashboardSrc> createState() => _DashboardSrcState();
}

class _DashboardSrcState extends State<DashboardSrc>  with SingleTickerProviderStateMixin{
  late TabController tabcontroller;

  int selectedIndex = 0;

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
    tabcontroller.animateTo(index, curve: Curves.linearToEaseOut);
  }
  var pages = [
    const ProfileSrc(),
    const UpdateProfileSrc(),
  ];

  @override
  void initState() {
    super.initState();
    tabcontroller = TabController(length: 2, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: TabBarView(
          controller: tabcontroller,
          physics: const NeverScrollableScrollPhysics(),
          children: pages,
        ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onItemTapped,
        currentIndex: selectedIndex,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Tài khoản',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervisor_account_sharp),
            label: 'Cập nhập',
          ),
        ],
        selectedItemColor: primaryColor,
        selectedLabelStyle: const TextStyle(fontSize: 14),
        unselectedLabelStyle:const TextStyle(fontSize: 10),
        selectedFontSize: 12,
        unselectedFontSize: 12,
      ),
    );
  }
}
