import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/add_address.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/edit_address.dart';

class ChooseAddressSrc extends StatefulWidget {
  const ChooseAddressSrc({super.key});

  @override
  State<ChooseAddressSrc> createState() => _ChooseAddressSrcState();
}

class CustomChoose extends StatelessWidget {
  final String? svgPath1, svgPath2, svgPath3;
  final String? textName, textAddress, textPhone;
  final bool isSelected;

  const CustomChoose(
      {super.key,
      this.svgPath1,
      this.svgPath2,
      this.svgPath3,
      this.textName,
      this.textAddress,
      this.textPhone,
      this.isSelected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath1"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textName",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath2"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textAddress",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath3"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textPhone",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

enum SingingCharacter { one, two, three, four }

class _ChooseAddressSrcState extends State<ChooseAddressSrc> {
  SingingCharacter? _character;

  @override
  void initState() {
    super.initState();
    _character = SingingCharacter.one;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const EditAddressSrc()));
              },
              child: const Text(
                "Sửa",
                style: TextStyle(color: Color(0xff0a6836)),
              ))
        ],
        title: const Text(
          "Chọn địa chỉ",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            ListView.builder(
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(8)),
                      border: Border.all(
                        color: _character == SingingCharacter.values[index]
                            ? const Color(0xff0a6836)
                            : Colors.grey,
                      )),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 8,
                          child: CustomChoose(
                            svgPath1: "assets/images/ion_person.svg",
                            svgPath2: "assets/images/mdi_location.svg",
                            svgPath3: "assets/images/mingcute_phone-fill.svg",
                            textName: "Nguyễn Tú Anh",
                            textAddress:
                                "185 Chùa Láng, Láng Thượng, Đống Đa, Hà Nội",
                            textPhone: "0914 xxx xxx",
                            isSelected:
                                _character == SingingCharacter.values[index],
                          )),
                      Expanded(
                        flex: 1,
                        child: Radio<SingingCharacter>(
                          activeColor: const Color(0xff0a6836),
                          value: SingingCharacter.values[index],
                          groupValue: _character,
                          onChanged: (SingingCharacter? value) {
                            setState(() {
                              _character = value!;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AddAddressSrc()));
              },
              child: CustomersButton(
                buttonText: "Thêm địa chỉ mới",
                width: MediaQuery.of(context).size.width,
                borderColor: const Color(0xff0a6836),
                bgcolor: const Color(0xff0a6836),
                textColor: const Color(0xfffffefa),
              ),
            )
          ],
        ),
      ),
    );
  }
}
