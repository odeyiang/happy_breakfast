
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../common_widget/const.dart';

class TransportSrc extends StatefulWidget {
  const TransportSrc({super.key});

  @override
  State<TransportSrc> createState() => _TransportSrcState();
}

enum SingingCharacter { one, two }

class _TransportSrcState extends State<TransportSrc> {
  SingingCharacter? _character = SingingCharacter.one;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Phương thức vận chuyển",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            Column(
              children: [
                Container(margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Radio<SingingCharacter>(
                          activeColor: const Color(0xff0a6836),
                          value: SingingCharacter.one,
                          groupValue: _character,
                          onChanged: (SingingCharacter? value) {
                            setState(() {
                              _character = value;
                            });
                          },
                        ),
                      ),
                      const Expanded(
                        flex: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Phí giao hàng mặc định",
                              style: TextStyle(fontWeight: FontWeight.w700),
                            ),
                            Text(
                                "Áp dụng với đơn hàng có trọng lượng dưới 3kg. Phí ship được tính thực tế với đơn hàng sỉ và đơn hàng lẻ có trọng lượng trên 3kg")
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Center(
                              child: Text(
                            "${Const.convertPrice(22000)}đ",
                            style: const TextStyle(fontWeight: FontWeight.w700),
                          ))),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Divider(
                  height: 1,
                  color: Color(0xffd1d1d1),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Radio<SingingCharacter>(
                            activeColor: const Color(0xff0a6836),
                            value: SingingCharacter.two,
                            groupValue: _character,
                            onChanged: (SingingCharacter? value) {
                              setState(() {
                                _character = value;
                              });
                            },
                          ),
                      ),
                      Expanded(
                        flex: 7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Hoả tốc",
                              style: TextStyle(fontWeight: FontWeight.w700),
                            ),
                            RichText(
                                text: TextSpan(children: [
                              const TextSpan(
                                  text:
                                      "Chọn để xem tình trạng còn hàng đối với ",
                                  style: TextStyle(color: Color(0xff393939))),
                              TextSpan(
                                text: "vị trí của bạn",
                                style: const TextStyle(
                                  color: Color(0xff0a6836),
                                  fontWeight: FontWeight.w500,
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    print('TextSpan clicked!');
                                  },
                              )
                            ]))
                          ],
                        ),
                      ),
                      // Expanded(flex: 2, child: Container()),
                    ],
                  ),
                ),
                const Divider(
                  height: 1,
                  color: Color(0xffd1d1d1),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
