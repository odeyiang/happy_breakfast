import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/add_address.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/choose_address_map.dart';

class EditAddressSrc extends StatefulWidget {
  const EditAddressSrc({super.key});

  @override
  State<EditAddressSrc> createState() => _EditAddressSrcState();
}

class CustomChoose extends StatelessWidget {
  final String? svgPath1, svgPath2, svgPath3;
  final String? textName, textAddress, textPhone;
  final bool isSelected;

  const CustomChoose(
      {super.key,
      this.svgPath1,
      this.svgPath2,
      this.svgPath3,
      this.textName,
      this.textAddress,
      this.textPhone,
      this.isSelected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath1"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textName",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath2"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textAddress",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1, child: Center(child: SvgPicture.asset("$svgPath3"))),
              Expanded(
                flex: 8,
                child: Text(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  "$textPhone",
                  //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _EditAddressSrcState extends State<EditAddressSrc> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                "Xong",
                style: TextStyle(color: Color(0xff0a6836)),
              ))
        ],
        title: const Text(
          "Sửa địa chỉ",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            ListView.builder(
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const ChooseAddressMapSrc()));
                  },
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        border: Border.all(
                          color: Colors.grey,
                        )),
                    child: const Row(
                      children: [
                        Expanded(
                            flex: 8,
                            child: CustomChoose(
                              svgPath1: "assets/images/ion_person.svg",
                              svgPath2: "assets/images/mdi_location.svg",
                              svgPath3: "assets/images/mingcute_phone-fill.svg",
                              textName: "Nguyễn Tú Anh",
                              textAddress:
                                  "185 Chùa Láng, Láng Thượng, Đống Đa, Hà Nội",
                              textPhone: "0914 xxx xxx",
                            )),
                        Expanded(
                          flex: 1,
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AddAddressSrc()));
              },
              child: CustomersButton(
                buttonText: "Thêm địa chỉ mới",
                width: MediaQuery.of(context).size.width,
                borderColor: const Color(0xff0a6836),
                bgcolor: const Color(0xff0a6836),
                textColor: const Color(0xfffffefa),
              ),
            )
          ],
        ),
      ),
    );
  }
}
