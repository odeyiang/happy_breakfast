
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';


class PaymentMethodSrc extends StatefulWidget {
  const PaymentMethodSrc({super.key});

  @override
  State<PaymentMethodSrc> createState() => _PaymentMethodSrcState();
}

enum SingingCharacter { one, two ,three}

class CustomChoose extends StatelessWidget {
  final String? svgPath;
  final String? textMethod;
  const CustomChoose({super.key, this.svgPath, this.textMethod});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 2, child: Center(child: SvgPicture.asset("$svgPath"))),
              Expanded(
                flex: 8,
                child: Text(
                  "$textMethod",
                //  style: const TextStyle(fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          const Divider(
            height: 1,
            color: Color(0xffd1d1d1),
          )
        ],
      ),
    );
  }
}

class _PaymentMethodSrcState extends State<PaymentMethodSrc> {
  SingingCharacter? _character = SingingCharacter.one;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Phương thức thanh toán",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            Row(
              children: [
                const Expanded(
                  flex: 8,
                  child: CustomChoose(
                    svgPath: "assets/images/money-bill.svg",
                    textMethod: "Thanh toán khi nhận hàng",
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Radio<SingingCharacter>(
                    activeColor: const Color(0xff0a6836),
                    value: SingingCharacter.one,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                const Expanded(
                  flex: 8,
                  child: CustomChoose(
                    svgPath: "assets/images/credit-card-plus.svg",
                    textMethod: "Thanh toán qua thẻ tín dụng",
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Radio<SingingCharacter>(
                    activeColor: const Color(0xff0a6836),
                    value: SingingCharacter.two,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value!;
                      });
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                const Expanded(
                  flex: 8,
                  child: CustomChoose(
                    svgPath: "assets/images/wallet-plus.svg",
                    textMethod: "Thanh toán qua ví",
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Radio<SingingCharacter>(
                    activeColor: const Color(0xff0a6836),
                    value: SingingCharacter.three,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value!;
                      });
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
