import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/buy_success.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/choose_address.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/payment_method.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/transport_method.dart';

import '../common_widget/const.dart';
import '../model/product_model.dart';

class PaymentSrc extends StatefulWidget {
  const PaymentSrc({super.key});

  @override
  State<PaymentSrc> createState() => _PaymentSrcState();
}

class _PaymentSrcState extends State<PaymentSrc> {
  Future<List<Product>> fetchData() async {
    String jsonData = await rootBundle.loadString('assets/product.json');

    List<dynamic> jsonList = json.decode(jsonData);
    List<Product> dataList =
        jsonList.map((json) => Product.fromJson(json)).toList();
    return dataList;
  }

  bool value = false;
  bool _isLoading = false;

  Widget buildList() {
    return FutureBuilder(
      future: fetchData(),
      builder: (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          List<Product> productList = snapshot.data!;
          return ListView.builder(
            itemCount: productList.length,
            itemBuilder: (context, index) {
              List<Product> productList = snapshot.data!;
              return Container(
                margin: const EdgeInsets.symmetric(vertical: 8),
                height: 120,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 16,
                    ),
                    Container(
                      height: 72,
                      width: 72,
                      margin: const EdgeInsets.only(top: 8),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                              image: AssetImage(
                                  "${productList[index].imagePath}"))),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 250,
                          child: Text(
                            "${productList[index].productName}",
                            style: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          height: 26,
                          width: 280,
                          decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Center(
                                  child: Text(
                                "Phân loại: ${productList[index].type}",
                                style: const TextStyle(
                                    color: Color(0xff1f1f1f),
                                    fontWeight: FontWeight.w400),
                              )),
                              const Text("x1")
                            ],
                          ),
                        ),
                        Text(
                          "${Const.convertPrice(productList[index].salePrice)}đ",
                          style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Color(0xff0a6836)),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          );
        }
      },
    );
  }

  Widget buildListVoucher() {
    return Container(
      height: 500,
      margin: const EdgeInsets.symmetric(horizontal: 16),
      padding: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const SizedBox(
            height: 20,
          ),
          const Center(
              child: Text(
            "Voucher giảm giá",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
          )),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: 10,
                itemBuilder: (BuildContext context, int index) {
                  return Row(
                    children: [
                      GestureDetector(
                        onTap:(){ showModalBottomSheet<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return buildDetailVoucher();
                            });},
                        child: Container(
                          margin: const EdgeInsets.symmetric(vertical: 8),
                          height: 80,
                          decoration: const BoxDecoration(),
                          child: Row(
                            children: [
                              Container(
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/voucher_bg.png"),
                                )),
                                child: Transform.scale(
                                    scale: 0.7,
                                    child: const Image(
                                      image: AssetImage(
                                          "assets/images/voucher_img.png"),
                                    )),
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                decoration: const BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                        color: Color(0xff858585),
                                        width: 0.2,
                                      ),
                                      top: BorderSide(
                                        color: Color(0xff858585),
                                        width: 0.2,
                                      ),
                                      bottom: BorderSide(
                                          color: Color(0xff858585), width: 0.2),
                                    ),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        bottomRight: Radius.circular(10))),
                                child: const Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Giảm 15% giảm tối đa 55k",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      "Đơn tối thiểu 300k",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      width: 200,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "HSD: 30/02/2023",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Color(0xff858585)),
                                          ),
                                          Text("Điều kiện",
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  color: Color(0xffb62c21)))
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Checkbox(
                        shape: const CircleBorder(),
                        value: value,
                        activeColor: const Color(0xff0a6836),
                        onChanged: (value) {
                          setState(() {
                            this.value = value!;
                          });
                        },
                      )
                    ],
                  );
                }),
          ),
          CustomersButton(
            borderColor: const Color(0xff0a6836),
            buttonText: "Dùng voucher",
            width: MediaQuery.of(context).size.width,
            bgcolor: const Color(0xff0a6836),
            textColor: const Color(0xfffffefa),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }

  Widget buildDetailVoucher() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      height: 500,
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          const Center(
            child: Text(
              "Chi tiết voucher giảm giá",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 350,
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 18),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.green, width: 0.3),
                borderRadius: const BorderRadius.all(Radius.circular(8))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 24),
                  height: 250,
                  child: const Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Image(image: AssetImage('assets/images/ship.png')),
                          SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Miễn phí vận chuyển",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff0a6836)),
                              ),
                              Text(
                                "Đơn tối thiểu đ0",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff9e9e9e)),
                              )
                            ],
                          )
                        ],
                      ),
                      Text(
                        "Giảm tối đa 55K",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.fiber_manual_record,
                            size: 8,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: Text(
                              "Sử dụng mã giảm giá vận chuyển cho đơn hàng thoả mãn điều kiện ưu đãi",
                              style: TextStyle(color: Color(0xff6b6b6b)),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.fiber_manual_record,
                            size: 8,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: Text(
                                "Áp dụng cho sản phẩm tham gia chương trình và người dùng nhất định",
                                style: TextStyle(color: Color(0xff6b6b6b))),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.fiber_manual_record,
                            size: 8,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: Text(
                                "Phương thức thanh toán: mọi hình thức thanh toán",
                                style: TextStyle(color: Color(0xff6b6b6b))),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Center(child: SvgPicture.asset("assets/images/memom.svg")),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SvgPicture.asset("assets/images/external_link.svg"),
                    const Text("HSD: 02/02/2024"),
                    const Icon(
                      Icons.error_outline,
                      color: Color(0xff0a6836),
                    )
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomersButton(
                  borderColor: const Color(0xff0a6836),
                  textColor: const Color(0xff0a6836),
                  buttonText: 'Quay lại',
                  width: 166,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                CustomersButton(
                  textColor: const Color(0xfffffefa),
                  borderColor: const Color(0xff0a6836),
                  bgcolor: const Color(0xff0a6836),
                  buttonText: 'Dùng voucher',
                  width: 166,
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Thanh toán",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              //áp mã thành công
              Container(
                height: 40,
                padding: const EdgeInsets.only(left: 8),
                decoration: const BoxDecoration(color: Color(0xfffff3db)),
                child: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Đã áp dụng phiếu giảm giá",
                      style: TextStyle(color: Color(0xff525252)),
                    )),
              ),
              const SizedBox(
                height: 26,
              ),
              //thông tin người mua
              GestureDetector(
                onTap: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ChooseAddressSrc()));
                },
                child: Container(
                  height: 100,
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  child: const Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.location_pin),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Nguyễn Tú Anh",
                            style: TextStyle(color: Colors.black),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "0982719167",
                            style: TextStyle(
                                color: Colors.black, fontWeight: FontWeight.w600),
                          ),
                          Icon(
                            Icons.play_arrow,
                            color: Colors.grey,
                          )
                        ],
                      ),
                      Text(
                        "Số 84, ngõ 66A, Triều Khúc, Thanh Trì, Hà Nội",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 36,
                child: Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                    SvgPicture.asset(
                      'assets/images/stripes.svg',
                    ),
                  ],
                ),
              ),
              // giỏ hàng
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 16),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.shopping_cart_outlined,
                          color: Color(0xff0a6836),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Giỏ hàng",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    Text("SL sản phẩm: 2")
                  ],
                ),
              ),
              // danh sách sản phẩm mua
              SizedBox(height: 265, child: buildList()),
              // tổng tiền
              Container(
                height: 250,
                decoration: const BoxDecoration(
                    border: Border(
                        top: BorderSide(color: Color(0xff0a6836)),
                        bottom: BorderSide(color: Color(0xff0a6836)))),
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Đơn vị vận chuyển (nhấn để chọn)",
                          style: TextStyle(color: Color(0xff0a6836)),
                        )),
                    //   const SizedBox(height: 16,),
                    const Text(
                      "Vận chuyển nhanh",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const TransportSrc()));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text("Phí giao hàng mặc định"),
                          Row(
                            children: [
                              Text("${Const.convertPrice(22000)}đ"),
                              const Icon(
                                Icons.play_arrow,
                                size: 14,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    const Text(
                      "Nhận hàng sau 1-2 ngày nội thành",
                      style: TextStyle(color: Color(0xff858585)),
                    ),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Tin nhắn"),
                        Text(
                          "Lưu ý cho người bán",
                          style: TextStyle(color: Color(0xffd1d1d1)),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Tổng số tiền (2 sản phẩm)",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        Text("${Const.convertPrice(800000)}đ",
                            style: const TextStyle(fontWeight: FontWeight.w700))
                      ],
                    )
                  ],
                ),
              ),
              // chọn voucher
              Container(
                height: 50,
                margin: const EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const SizedBox(
                      child: Row(
                        children: [
                          Icon(Icons.confirmation_number_outlined,
                              color: Color(0xff0a6836)),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Voucher của bạn",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        showModalBottomSheet<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return buildListVoucher();
                          },
                        );
                      },
                      child: Container(
                          height: 20,
                          width: 140,
                          margin: const EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color(0xff0a6836), width: 0.2)),
                          child: const Center(
                            child: Text(
                              "Giảm 10% mã vận chuyển",
                              style: TextStyle(
                                  color: Color(0xff0a6836), fontSize: 10),
                            ),
                          )),
                    )
                  ],
                ),
              ),
              // chọn phương thức thanh toán
              Container(
                height: 60,
                margin: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Row(
                      children: [
                        Icon(
                          Icons.monetization_on_outlined,
                          color: Color(0xff0a6836),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Phương thức thanh toán",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const PaymentMethodSrc()));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 30),
                            child: SvgPicture.asset(
                              'assets/images/payment.svg',
                            ),
                          ),
                          const Text(
                            "Thanh toán khi nhận hàng",
                            style: TextStyle(color: Color(0xff0a6836)),
                          ),
                          const Icon(
                            Icons.play_arrow,
                            size: 14,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // điểm thưởng
              Container(
                margin: const EdgeInsets.all(16),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.card_giftcard_outlined,
                          color: Color(0xff0a6836),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Điểm thưởng",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                    Text(
                      "+1000 điểm",
                      style: TextStyle(
                          color: Color(0xffb62c21),
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              // bonus
              Container(
                margin: const EdgeInsets.all(16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Icon(
                      Icons.error,
                      color: Color(0xffb62c21),
                      size: 14,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            text: const TextSpan(children: [
                          TextSpan(
                              text: "Mua thêm ",
                              style: TextStyle(
                                  color: Color(0xff1f1f1f), fontSize: 12)),
                          TextSpan(
                              text: "500000đ ",
                              style: TextStyle(
                                  color: Color(0xffda3e31), fontSize: 12)),
                          TextSpan(
                              text: "để sử dụng phiếu giảm giá 10% ",
                              style: TextStyle(
                                  fontSize: 12, color: Color(0xff1f1f1f))),
                        ])),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          child: RichText(
                              text: const TextSpan(children: [
                            TextSpan(
                                text: "Tổng cộng ",
                                style: TextStyle(
                                    color: Color(0xff1f1f1f), fontSize: 12)),
                            TextSpan(
                                text: "+1000 ",
                                style: TextStyle(
                                    color: Color(0xffda3e31), fontSize: 12)),
                            TextSpan(
                                text:
                                    "điểm nhận được, có thể sử dụng khi điểm\nlớn hơn ",
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff1f1f1f))),
                            TextSpan(
                                text: "1000 ",
                                style: TextStyle(
                                    color: Color(0xffb62c21),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700)),
                          ])),
                        )
                      ],
                    )
                  ],
                ),
              ),
              // tổng thanh toán
              Container(
                height: 250,
                margin: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/memo.svg',
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          "Thông tin thanh toán",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Tổng tiền hàng",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("${Const.convertPrice(800000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Tổng tiền vận chuyển",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("+${Const.convertPrice(22000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Mã giảm giá",
                            style: TextStyle(fontWeight: FontWeight.w400)),
                        Text("-${Const.convertPrice(50000)}đ")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Tổng thanh toán",
                            style: TextStyle(fontWeight: FontWeight.w500)),
                        Text(
                          "${Const.convertPrice(772000)}đ",
                          style: const TextStyle(
                              color: Color(0xff0a6836),
                              fontWeight: FontWeight.w700,
                              fontSize: 18),
                        )
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset("assets/images/note.svg"),
                        const SizedBox(
                          width: 5,
                        ),
                        const Expanded(
                          child: Text(
                            "Nhấn 'đặt hàng' đồng nghĩa với việc bạn đồng ý tuân theo Điều khoản Happy Breakfast ",
                            style: TextStyle(fontSize: 16),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              // button đặt hàng
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomersButton(
                      borderColor: const Color(0xff0a6836),
                      textColor: const Color(0xff0a6836),
                      buttonText: 'Huỷ',
                      width: 166,
                      onPressed: () {},
                    ),
                    CustomersButton(
                      textColor: const Color(0xfffffefa),
                      borderColor: const Color(0xff0a6836),
                      bgcolor: const Color(0xff0a6836),
                      buttonText: 'Đặt hàng',
                      width: 166,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const BuySuccessSrc()),
                        );
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => const BuySuccessSrc()));
                      },
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              )
            ],
          ),
        ),
      ),
    );
  }
}
