import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/payment/map.dart';

class ChooseAddressMapSrc extends StatefulWidget {
  const ChooseAddressMapSrc({super.key});

  @override
  State<ChooseAddressMapSrc> createState() => _ChooseAddressMapSrcState();
}

class ItemAddress extends StatelessWidget {
  final String nameAddress;
  final String path;
  final Color? textColor;
  final Color borderColor;
  final bool? isClick;

  const ItemAddress({
    super.key,
    required this.nameAddress,
    required this.path,
    this.textColor,
    required this.borderColor,
    this.isClick = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width * 0.28,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      decoration: BoxDecoration(
          border: Border.all(color: borderColor),
          borderRadius: const BorderRadius.all(Radius.circular(4))),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: SvgPicture.asset(
                path,
              )),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            flex: 3,
            child: Text(
              nameAddress,
              style: TextStyle(color: textColor, fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }
}

enum ClickAddress { home, company, other }

class _ChooseAddressMapSrcState extends State<ChooseAddressMapSrc> {
  bool show = false;
  ClickAddress? clickAddress;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Sửa địa chỉ",
          style: TextStyle(fontSize: 24),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: const BoxDecoration(
                  border: Border(
                bottom: BorderSide(width: 0.5, color: Colors.grey),
              )),
              child: Row(
                children: [
                  SvgPicture.asset("assets/images/ion_person.svg"),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    child: const Text("|"),
                  ),
                  const Text(
                    "Nguyễn Tú Anh",
                    style: TextStyle(fontSize: 12),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: const BoxDecoration(
                  border: Border(
                bottom: BorderSide(width: 0.5, color: Colors.grey),
              )),
              child: Row(
                children: [
                  SvgPicture.asset("assets/images/mingcute_phone-fill.svg"),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    child: const Text("|"),
                  ),
                  const Text("0914 xxx xxx", style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: const BoxDecoration(
                  border: Border(
                bottom: BorderSide(width: 0.5, color: Colors.grey),
              )),
              child: Row(
                children: [
                  SvgPicture.asset("assets/images/mdi_location.svg"),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    child: const Text("|"),
                  ),
                  const Text("185 Chùa Láng, Láng Thượng, Đống Đa, Hà Nội",
                      style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const MapSrc()));
              },
              child: Container(
                color: Colors.green,
                height: 400,
              ),
            ),
            const Text(
              "Loại địa chỉ",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      clickAddress = ClickAddress.home;
                    });
                  },
                  child: ItemAddress(
                    nameAddress: "Nhà",
                    path: "assets/images/home_address.svg",
                    borderColor: clickAddress == ClickAddress.home
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                    textColor: clickAddress == ClickAddress.home
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      clickAddress = ClickAddress.company;
                    });
                  },
                  child: ItemAddress(
                    nameAddress: "Công ty",
                    path: "assets/images/company_address.svg",
                    borderColor: clickAddress == ClickAddress.company
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                    textColor: clickAddress == ClickAddress.company
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      clickAddress = ClickAddress.other;
                    });
                  },
                  child: ItemAddress(
                    nameAddress: "Khác",
                    path: "assets/images/location_address.svg",
                    borderColor: clickAddress == ClickAddress.other
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                    textColor: clickAddress == ClickAddress.other
                        ? const Color(0xff0a6836)
                        : Colors.grey,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              const Text(
                "Đặt làm địa chỉ mặc định",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
              Switch(
                  activeColor: const Color(0xff26a44d),
                  value: show,
                  inactiveThumbColor: Colors.white,
                  onChanged: (bool value) {
                    setState(() {
                      show = value;
                    });
                  })
            ]),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: const CustomersButton(
                    buttonText: "Xoá địa chỉ",
                    width: 166,
                    borderColor: Color(0xff0a6836),
                    textColor: Color(0xff0a6836),
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: const CustomersButton(
                    buttonText: "Xác nhận",
                    width: 166,
                    borderColor: Color(0xff0a6836),
                    textColor: Color(0xfffffefa),
                    bgcolor: Color(0xff0a6838),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
