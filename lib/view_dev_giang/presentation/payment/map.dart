import 'package:flutter/material.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';

class MapSrc extends StatefulWidget {
  const MapSrc({super.key});

  @override
  State<MapSrc> createState() => _MapSrcState();
}

class _MapSrcState extends State<MapSrc> {
  TextEditingController name = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Chọn địa chỉ",
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 500,
              width: MediaQuery.of(context).size.width,
              // child:GoogleMap(
              //   initialCameraPosition: const CameraPosition(
              //     target: LatLng(20.633874266360287, 106.33598156689656),
              //     zoom: 20,
              //   ),
              //   onMapCreated: (GoogleMapController controller) {
              //   },
              // ),
            ),
            const Text(
              "Chọn địa chỉ",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Nhập địa chỉ của bạn",
              style: TextStyle(fontSize: 16, color: Color(0xff9e9e9e)),
            ),
            TextField(
              controller: name,
              keyboardType: TextInputType.name,
              style: const TextStyle(fontSize: 14),
              cursorColor: const Color(0xff0a6836),
              decoration: const InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff0a6836)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff0a6836)),
                ),
                hintText: "Nhập địa chỉ",
                hintStyle: TextStyle(color: Color(0xff919696), fontSize: 14),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {},
              child: Center(
                child: CustomersButton(
                  buttonText: "Xác nhận",
                  width: MediaQuery.of(context).size.width,
                  borderColor: const Color(0xff0a6836),
                  textColor: const Color(0xfffffefa),
                  bgcolor: const Color(0xff0a6838),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
