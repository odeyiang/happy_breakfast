import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_button.dart';
import 'package:happy_breakfast/view_dev_giang/presentation/common_widget/custom_card.dart';


class BuySuccessSrc extends StatefulWidget {
  const BuySuccessSrc({super.key});

  @override
  State<BuySuccessSrc> createState() => _BuySuccessSrcState();
}

class _BuySuccessSrcState extends State<BuySuccessSrc> {
  bool _isOverlayVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/confetti.png"),
                scale: 0.5,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset("assets/images/box_success.svg"),
                const SizedBox(height: 20),
                const Text(
                  "Bạn đã đặt hàng thành công!",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomersButton(
                      borderColor: const Color(0xff0a6836),
                      textColor: const Color(0xff0a6836),
                      buttonText: 'Về trang chủ',
                      width: 166,
                      onPressed: () {},
                    ),
                    CustomersButton(
                      textColor: const Color(0xfffffefa),
                      borderColor: const Color(0xff0a6836),
                      bgcolor: const Color(0xff0a6836),
                      buttonText: 'Tiếp tục mua hàng',
                      width: 166,
                      onPressed: () {},
                    ),
                  ],
                ),
              ],
            ),
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 300),
            bottom: _isOverlayVisible ? 0 : -600,
            left: 0,
            right: 0,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _isOverlayVisible = !_isOverlayVisible;
                });
              },
              child: Container(
                height: 700,
                decoration: BoxDecoration(
                 // color: const Color(0xfffffefa),
                  border: Border.all(width: 0.1),
                  borderRadius:const BorderRadius.all(Radius.circular(8))
                ),
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text("Sản phầm tương tự",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 16)),
                        Text("Có thể bạn sẽ thích",
                            style: TextStyle(
                                fontSize: 12, color: Color(0xff919696))),
                      ],
                    ),
                    Expanded(
                      child: GridView.builder(
                        primary: false,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio:0.7,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                        ),
                        shrinkWrap: true,
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return const CustomCardProduct(
                            text: 'Goi 10 bua sang dinh duong va thanh loc (10 packs)',
                            imagePath: "assets/images/product.png",
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
